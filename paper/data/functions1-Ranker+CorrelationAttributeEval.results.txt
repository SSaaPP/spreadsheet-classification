=== Evaluation result ===

Scheme: NaiveBayesMultinomial
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1


Correctly Classified Instances         847               20.454  %
Incorrectly Classified Instances      3294               79.546  %
Kappa statistic                          0.031 
Mean absolute error                      0.2739
Root mean squared error                  0.3705
Relative absolute error                 98.6389 %
Root relative squared error             99.4267 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.024    0.006    0.421      0.024    0.045      0.068    0.536     0.185     database
                 0.979    0.927    0.188      0.979    0.316      0.081    0.530     0.195     financial
                 0.068    0.013    0.516      0.068    0.121      0.141    0.550     0.216     grades
                 0.068    0.008    0.597      0.068    0.121      0.163    0.531     0.212     homework
                 0.003    0.006    0.087      0.003    0.006      -0.016   0.519     0.174     inventory
                 0.013    0.009    0.220      0.013    0.024      0.013    0.513     0.176     modeling
Weighted Avg.    0.205    0.174    0.332      0.205    0.108      0.074    0.530     0.193     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
  16 641   2   3   3   4 |   a = database
   3 730   1   2   3   7 |   b = financial
   2 627  47   5   7   1 |   c = grades
   7 545  30  43   1  11 |   d = homework
   8 661   7   8   2   9 |   e = inventory
   2 672   4  11   7   9 |   f = modeling
=== Evaluation result ===

Scheme: J48
Options: -C 0.25 -M 2
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1


Correctly Classified Instances         781               18.8602 %
Incorrectly Classified Instances      3360               81.1398 %
Kappa statistic                          0.0107
Mean absolute error                      0.2763
Root mean squared error                  0.3717
Relative absolute error                 99.5052 %
Root relative squared error             99.7716 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.000    0.000    0.000      0.000    0.000      0.000    0.506     0.163     database
                 1.000    0.984    0.183      1.000    0.309      0.054    0.508     0.182     financial
                 0.016    0.001    0.733      0.016    0.031      0.092    0.515     0.179     grades
                 0.038    0.004    0.615      0.038    0.071      0.125    0.515     0.174     homework
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.167     inventory
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.170     modeling
Weighted Avg.    0.189    0.178    0.250      0.189    0.072      0.044    0.507     0.173     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
   0 668   1   0   0   0 |   a = database
   0 746   0   0   0   0 |   b = financial
   0 677  11   1   0   0 |   c = grades
   0 610   3  24   0   0 |   d = homework
   0 688   0   7   0   0 |   e = inventory
   0 698   0   7   0   0 |   f = modeling
=== Evaluation result ===

Scheme: NaiveBayes
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1


Correctly Classified Instances         845               20.4057 %
Incorrectly Classified Instances      3296               79.5943 %
Kappa statistic                          0.0303
Mean absolute error                      0.2766
Root mean squared error                  0.3716
Relative absolute error                 99.6092 %
Root relative squared error             99.7334 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.015    0.002    0.625      0.015    0.029      0.078    0.533     0.185     database
                 0.984    0.941    0.187      0.984    0.314      0.075    0.529     0.195     financial
                 0.061    0.011    0.532      0.061    0.109      0.137    0.542     0.208     grades
                 0.078    0.010    0.588      0.078    0.139      0.174    0.538     0.213     homework
                 0.001    0.002    0.111      0.001    0.003      -0.007   0.513     0.173     inventory
                 0.011    0.004    0.348      0.011    0.022      0.035    0.511     0.176     modeling
Weighted Avg.    0.204    0.174    0.391      0.204    0.105      0.081    0.528     0.191     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
  10 650   1   4   1   3 |   a = database
   1 734   2   4   1   4 |   b = financial
   1 634  42   7   3   2 |   c = grades
   0 561  25  50   0   1 |   d = homework
   4 671   5   9   1   5 |   e = inventory
   0 679   4  11   3   8 |   f = modeling
=== Evaluation result ===

Scheme: REPTree
Options: -M 2 -V 0.001 -N 3 -S 1 -L -1 -I 0.0
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1


Correctly Classified Instances         830               20.0435 %
Incorrectly Classified Instances      3311               79.9565 %
Kappa statistic                          0.0257
Mean absolute error                      0.2737
Root mean squared error                  0.371 
Relative absolute error                 98.5677 %
Root relative squared error             99.583  %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.013    0.002    0.600      0.013    0.026      0.072    0.520     0.173     database
                 0.987    0.947    0.186      0.987    0.313      0.073    0.518     0.189     financial
                 0.054    0.011    0.487      0.054    0.097      0.118    0.535     0.201     grades
                 0.063    0.009    0.563      0.063    0.113      0.150    0.533     0.199     homework
                 0.000    0.002    0.000      0.000    0.000      -0.018   0.511     0.173     inventory
                 0.011    0.004    0.381      0.011    0.022      0.040    0.509     0.177     modeling
Weighted Avg.    0.200    0.175    0.363      0.200    0.098      0.071    0.521     0.185     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
   9 651   1   3   1   4 |   a = database
   0 736   1   2   2   5 |   b = financial
   1 638  37  10   3   0 |   c = grades
   1 571  25  40   0   0 |   d = homework
   4 672   8   7   0   4 |   e = inventory
   0 683   4   9   1   8 |   f = modeling
=== Evaluation result ===

Scheme: DecisionTable
Options: -X 1 -S "weka.attributeSelection.BestFirst -D 1 -N 5"
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1


Correctly Classified Instances         801               19.3432 %
Incorrectly Classified Instances      3340               80.6568 %
Kappa statistic                          0.0168
Mean absolute error                      0.276 
Root mean squared error                  0.3714
Relative absolute error                 99.4159 %
Root relative squared error             99.67   %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.003    0.001    0.333      0.003    0.006      0.018    0.512     0.165     database
                 0.997    0.970    0.184      0.997    0.311      0.068    0.512     0.184     financial
                 0.028    0.005    0.528      0.028    0.052      0.091    0.518     0.187     grades
                 0.055    0.007    0.583      0.055    0.100      0.144    0.531     0.200     homework
                 0.000    0.000    0.000      0.000    0.000      0.000    0.505     0.170     inventory
                 0.001    0.001    0.333      0.001    0.003      0.012    0.509     0.173     modeling
Weighted Avg.    0.193    0.177    0.321      0.193    0.082      0.054    0.514     0.179     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
   2 664   1   2   0   0 |   a = database
   0 744   0   2   0   0 |   b = financial
   2 662  19   6   0   0 |   c = grades
   1 587  14  35   0   0 |   d = homework
   1 684   1   7   0   2 |   e = inventory
   0 695   1   8   0   1 |   f = modeling
=== Evaluation result ===

Scheme: NaiveBayesMultinomial
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1-weka.filters.unsupervised.attribute.Remove-V-R72,22,117,112,47,93,69,116,21,100-101,121,44,50,80,59,19,4,130,78,119,76,62,66,30,90,105,12,94,97,115,67,53,27,134,99,64,125,57,23,55,131,127,56,83,92,124,11,52,33,6,1,108,10,132,31,95,84,68,5,133,82,18,88,2,98,91,102,85,129,126,86,107,34,40,35,39,49,14-15,24,20,8,65,7,28,81,128,13,63,79,73,61,25,46,70,87,138,137,106,103,17,60,96,123,29,32,136,89,77,114,111,45,43,48,71,74,109,104,113,26,110,51,75,41,38,36,42,37,3,120,135,16,118,9,122,58,54,139


Correctly Classified Instances         847               20.454  %
Incorrectly Classified Instances      3294               79.546  %
Kappa statistic                          0.031 
Mean absolute error                      0.2739
Root mean squared error                  0.3705
Relative absolute error                 98.6389 %
Root relative squared error             99.4267 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.024    0.006    0.421      0.024    0.045      0.068    0.536     0.185     database
                 0.979    0.927    0.188      0.979    0.316      0.081    0.530     0.195     financial
                 0.068    0.013    0.516      0.068    0.121      0.141    0.550     0.216     grades
                 0.068    0.008    0.597      0.068    0.121      0.163    0.531     0.212     homework
                 0.003    0.006    0.087      0.003    0.006      -0.016   0.519     0.174     inventory
                 0.013    0.009    0.220      0.013    0.024      0.013    0.513     0.176     modeling
Weighted Avg.    0.205    0.174    0.332      0.205    0.108      0.074    0.530     0.193     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
  16 641   2   3   3   4 |   a = database
   3 730   1   2   3   7 |   b = financial
   2 627  47   5   7   1 |   c = grades
   7 545  30  43   1  11 |   d = homework
   8 661   7   8   2   9 |   e = inventory
   2 672   4  11   7   9 |   f = modeling
=== Evaluation result ===

Scheme: J48
Options: -C 0.25 -M 2
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1-weka.filters.unsupervised.attribute.Remove-V-R72,22,117,112,47,93,69,116,21,100-101,121,44,50,80,59,19,4,130,78,119,76,62,66,30,90,105,12,94,97,115,67,53,27,134,99,64,125,57,23,55,131,127,56,83,92,124,11,52,33,6,1,108,10,132,31,95,84,68,5,133,82,18,88,2,98,91,102,85,129,126,86,107,34,40,35,39,49,14-15,24,20,8,65,7,28,81,128,13,63,79,73,61,25,46,70,87,138,137,106,103,17,60,96,123,29,32,136,89,77,114,111,45,43,48,71,74,109,104,113,26,110,51,75,41,38,36,42,37,3,120,135,16,118,9,122,58,54,139


Correctly Classified Instances         781               18.8602 %
Incorrectly Classified Instances      3360               81.1398 %
Kappa statistic                          0.0107
Mean absolute error                      0.2763
Root mean squared error                  0.3717
Relative absolute error                 99.5052 %
Root relative squared error             99.7716 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.000    0.000    0.000      0.000    0.000      0.000    0.506     0.163     database
                 1.000    0.984    0.183      1.000    0.309      0.054    0.508     0.182     financial
                 0.016    0.001    0.733      0.016    0.031      0.092    0.515     0.179     grades
                 0.038    0.004    0.615      0.038    0.071      0.125    0.515     0.174     homework
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.167     inventory
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.170     modeling
Weighted Avg.    0.189    0.178    0.250      0.189    0.072      0.044    0.507     0.173     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
   0 668   1   0   0   0 |   a = database
   0 746   0   0   0   0 |   b = financial
   0 677  11   1   0   0 |   c = grades
   0 610   3  24   0   0 |   d = homework
   0 688   0   7   0   0 |   e = inventory
   0 698   0   7   0   0 |   f = modeling
=== Evaluation result ===

Scheme: NaiveBayes
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1-weka.filters.unsupervised.attribute.Remove-V-R72,22,117,112,47,93,69,116,21,100-101,121,44,50,80,59,19,4,130,78,119,76,62,66,30,90,105,12,94,97,115,67,53,27,134,99,64,125,57,23,55,131,127,56,83,92,124,11,52,33,6,1,108,10,132,31,95,84,68,5,133,82,18,88,2,98,91,102,85,129,126,86,107,34,40,35,39,49,14-15,24,20,8,65,7,28,81,128,13,63,79,73,61,25,46,70,87,138,137,106,103,17,60,96,123,29,32,136,89,77,114,111,45,43,48,71,74,109,104,113,26,110,51,75,41,38,36,42,37,3,120,135,16,118,9,122,58,54,139


Correctly Classified Instances         845               20.4057 %
Incorrectly Classified Instances      3296               79.5943 %
Kappa statistic                          0.0303
Mean absolute error                      0.2766
Root mean squared error                  0.3716
Relative absolute error                 99.6092 %
Root relative squared error             99.7334 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.015    0.002    0.625      0.015    0.029      0.078    0.533     0.185     database
                 0.984    0.941    0.187      0.984    0.314      0.075    0.529     0.195     financial
                 0.061    0.011    0.532      0.061    0.109      0.137    0.542     0.208     grades
                 0.078    0.010    0.588      0.078    0.139      0.174    0.538     0.213     homework
                 0.001    0.002    0.111      0.001    0.003      -0.007   0.513     0.173     inventory
                 0.011    0.004    0.348      0.011    0.022      0.035    0.511     0.176     modeling
Weighted Avg.    0.204    0.174    0.391      0.204    0.105      0.081    0.528     0.191     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
  10 650   1   4   1   3 |   a = database
   1 734   2   4   1   4 |   b = financial
   1 634  42   7   3   2 |   c = grades
   0 561  25  50   0   1 |   d = homework
   4 671   5   9   1   5 |   e = inventory
   0 679   4  11   3   8 |   f = modeling
=== Evaluation result ===

Scheme: REPTree
Options: -M 2 -V 0.001 -N 3 -S 1 -L -1 -I 0.0
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1-weka.filters.unsupervised.attribute.Remove-V-R72,22,117,112,47,93,69,116,21,100-101,121,44,50,80,59,19,4,130,78,119,76,62,66,30,90,105,12,94,97,115,67,53,27,134,99,64,125,57,23,55,131,127,56,83,92,124,11,52,33,6,1,108,10,132,31,95,84,68,5,133,82,18,88,2,98,91,102,85,129,126,86,107,34,40,35,39,49,14-15,24,20,8,65,7,28,81,128,13,63,79,73,61,25,46,70,87,138,137,106,103,17,60,96,123,29,32,136,89,77,114,111,45,43,48,71,74,109,104,113,26,110,51,75,41,38,36,42,37,3,120,135,16,118,9,122,58,54,139


Correctly Classified Instances         829               20.0193 %
Incorrectly Classified Instances      3312               79.9807 %
Kappa statistic                          0.0254
Mean absolute error                      0.2737
Root mean squared error                  0.3711
Relative absolute error                 98.5746 %
Root relative squared error             99.6097 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.013    0.002    0.600      0.013    0.026      0.072    0.520     0.173     database
                 0.987    0.947    0.186      0.987    0.313      0.073    0.519     0.189     financial
                 0.054    0.011    0.487      0.054    0.097      0.118    0.535     0.201     grades
                 0.063    0.009    0.563      0.063    0.113      0.150    0.533     0.199     homework
                 0.000    0.002    0.000      0.000    0.000      -0.018   0.511     0.173     inventory
                 0.010    0.004    0.333      0.010    0.019      0.031    0.507     0.174     modeling
Weighted Avg.    0.200    0.175    0.355      0.200    0.097      0.070    0.521     0.185     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
   9 651   1   3   1   4 |   a = database
   0 736   1   2   2   5 |   b = financial
   1 638  37  10   3   0 |   c = grades
   1 570  25  40   0   1 |   d = homework
   4 672   8   7   0   4 |   e = inventory
   0 684   4   9   1   7 |   f = modeling
=== Evaluation result ===

Scheme: DecisionTable
Options: -X 1 -S "weka.attributeSelection.BestFirst -D 1 -N 5"
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1-weka.filters.unsupervised.attribute.Remove-V-R72,22,117,112,47,93,69,116,21,100-101,121,44,50,80,59,19,4,130,78,119,76,62,66,30,90,105,12,94,97,115,67,53,27,134,99,64,125,57,23,55,131,127,56,83,92,124,11,52,33,6,1,108,10,132,31,95,84,68,5,133,82,18,88,2,98,91,102,85,129,126,86,107,34,40,35,39,49,14-15,24,20,8,65,7,28,81,128,13,63,79,73,61,25,46,70,87,138,137,106,103,17,60,96,123,29,32,136,89,77,114,111,45,43,48,71,74,109,104,113,26,110,51,75,41,38,36,42,37,3,120,135,16,118,9,122,58,54,139


Correctly Classified Instances         801               19.3432 %
Incorrectly Classified Instances      3340               80.6568 %
Kappa statistic                          0.0168
Mean absolute error                      0.276 
Root mean squared error                  0.3714
Relative absolute error                 99.4159 %
Root relative squared error             99.67   %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.003    0.001    0.333      0.003    0.006      0.018    0.512     0.165     database
                 0.997    0.970    0.184      0.997    0.311      0.068    0.512     0.184     financial
                 0.028    0.005    0.528      0.028    0.052      0.091    0.518     0.187     grades
                 0.055    0.007    0.583      0.055    0.100      0.144    0.531     0.200     homework
                 0.000    0.000    0.000      0.000    0.000      0.000    0.505     0.170     inventory
                 0.001    0.001    0.333      0.001    0.003      0.012    0.509     0.173     modeling
Weighted Avg.    0.193    0.177    0.321      0.193    0.082      0.054    0.514     0.179     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
   2 664   1   2   0   0 |   a = database
   0 744   0   2   0   0 |   b = financial
   2 662  19   6   0   0 |   c = grades
   1 587  14  35   0   0 |   d = homework
   1 684   1   7   0   2 |   e = inventory
   0 695   1   8   0   1 |   f = modeling
