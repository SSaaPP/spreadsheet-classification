=== Evaluation result ===

Scheme: NaiveBayes
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1-weka.filters.unsupervised.attribute.Remove-V-R22,59,69,72,117,139


Correctly Classified Instances         804               19.4156 %
Incorrectly Classified Instances      3337               80.5844 %
Kappa statistic                          0.0177
Mean absolute error                      0.2769
Root mean squared error                  0.3719
Relative absolute error                 99.7422 %
Root relative squared error             99.8221 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.007    0.000    0.833      0.007    0.015      0.070    0.515     0.170     database
                 1.000    0.971    0.185      1.000    0.312      0.073    0.514     0.184     financial
                 0.028    0.005    0.528      0.028    0.052      0.091    0.515     0.185     grades
                 0.053    0.006    0.607      0.053    0.098      0.147    0.517     0.184     homework
                 0.000    0.000    0.000      0.000    0.000      0.000    0.506     0.169     inventory
                 0.000    0.000    0.000      0.000    0.000      0.000    0.507     0.172     modeling
Weighted Avg.    0.194    0.177    0.349      0.194    0.082      0.062    0.512     0.177     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
   5 663   1   0   0   0 |   a = database
   0 746   0   0   0   0 |   b = financial
   0 662  19   8   0   0 |   c = grades
   1 587  15  34   0   0 |   d = homework
   0 687   1   7   0   0 |   e = inventory
   0 698   0   7   0   0 |   f = modeling
=== Evaluation result ===

Scheme: J48
Options: -C 0.25 -M 2
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1-weka.filters.unsupervised.attribute.Remove-V-R22,59,69,72,117,139


Correctly Classified Instances         781               18.8602 %
Incorrectly Classified Instances      3360               81.1398 %
Kappa statistic                          0.0107
Mean absolute error                      0.2763
Root mean squared error                  0.3718
Relative absolute error                 99.5192 %
Root relative squared error             99.7809 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.000    0.000    0.000      0.000    0.000      0.000    0.506     0.163     database
                 1.000    0.984    0.183      1.000    0.309      0.054    0.508     0.182     financial
                 0.016    0.001    0.733      0.016    0.031      0.092    0.515     0.179     grades
                 0.038    0.004    0.615      0.038    0.071      0.125    0.515     0.174     homework
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.167     inventory
                 0.000    0.000    0.000      0.000    0.000      0.000    0.500     0.170     modeling
Weighted Avg.    0.189    0.178    0.250      0.189    0.072      0.044    0.507     0.173     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
   0 668   1   0   0   0 |   a = database
   0 746   0   0   0   0 |   b = financial
   0 677  11   1   0   0 |   c = grades
   0 610   3  24   0   0 |   d = homework
   0 688   0   7   0   0 |   e = inventory
   0 698   0   7   0   0 |   f = modeling
=== Evaluation result ===

Scheme: NaiveBayesMultinomial
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1-weka.filters.unsupervised.attribute.Remove-V-R22,59,69,72,117,139


Correctly Classified Instances         785               18.9568 %
Incorrectly Classified Instances      3356               81.0432 %
Kappa statistic                          0.0118
Mean absolute error                      0.2768
Root mean squared error                  0.3718
Relative absolute error                 99.6956 %
Root relative squared error             99.8001 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.007    0.000    0.833      0.007    0.015      0.070    0.516     0.172     database
                 1.000    0.975    0.184      1.000    0.311      0.068    0.505     0.183     financial
                 0.030    0.005    0.553      0.030    0.058      0.100    0.520     0.189     grades
                 0.011    0.000    0.875      0.011    0.022      0.088    0.513     0.187     homework
                 0.001    0.001    0.250      0.001    0.003      0.007    0.505     0.170     inventory
                 0.007    0.007    0.167      0.007    0.014      -0.001   0.509     0.173     modeling
Weighted Avg.    0.190    0.178    0.465      0.190    0.074      0.055    0.511     0.179     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
   5 663   1   0   0   0 |   a = database
   0 746   0   0   0   0 |   b = financial
   0 666  21   1   0   1 |   c = grades
   1 595  15   7   1  18 |   d = homework
   0 687   1   0   1   6 |   e = inventory
   0 698   0   0   2   5 |   f = modeling
=== Evaluation result ===

Scheme: REPTree
Options: -M 2 -V 0.001 -N 3 -S 1 -L -1 -I 0.0
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1-weka.filters.unsupervised.attribute.Remove-V-R22,59,69,72,117,139


Correctly Classified Instances         803               19.3915 %
Incorrectly Classified Instances      3338               80.6085 %
Kappa statistic                          0.0174
Mean absolute error                      0.2749
Root mean squared error                  0.371 
Relative absolute error                 99.0221 %
Root relative squared error             99.5595 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.004    0.000    0.750      0.004    0.009      0.050    0.514     0.168     database
                 1.000    0.972    0.184      1.000    0.311      0.072    0.513     0.184     financial
                 0.029    0.005    0.541      0.029    0.055      0.095    0.516     0.186     grades
                 0.053    0.006    0.618      0.053    0.098      0.149    0.526     0.200     homework
                 0.000    0.000    0.000      0.000    0.000      0.000    0.501     0.168     inventory
                 0.000    0.000    0.000      0.000    0.000      0.000    0.505     0.171     modeling
Weighted Avg.    0.194    0.177    0.339      0.194    0.082      0.060    0.512     0.179     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
   3 665   1   0   0   0 |   a = database
   0 746   0   0   0   0 |   b = financial
   0 662  20   7   0   0 |   c = grades
   1 587  15  34   0   0 |   d = homework
   0 687   1   7   0   0 |   e = inventory
   0 698   0   7   0   0 |   f = modeling
=== Evaluation result ===

Scheme: DecisionTable
Options: -X 1 -S "weka.attributeSelection.BestFirst -D 1 -N 5"
Relation: spreadsheets-weka.filters.unsupervised.attribute.Reorder-R2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,84,85,86,87,88,89,90,91,92,93,94,95,96,97,98,99,100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,1-weka.filters.unsupervised.attribute.Remove-V-R22,59,69,72,117,139


Correctly Classified Instances         799               19.2949 %
Incorrectly Classified Instances      3342               80.7051 %
Kappa statistic                          0.0162
Mean absolute error                      0.276 
Root mean squared error                  0.3713
Relative absolute error                 99.3974 %
Root relative squared error             99.6436 %
Total Number of Instances             4141     

=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0.001    0.001    0.250      0.001    0.003      0.007    0.512     0.165     database
                 1.000    0.974    0.184      1.000    0.311      0.069    0.513     0.184     financial
                 0.026    0.004    0.581      0.026    0.050      0.097    0.519     0.188     grades
                 0.053    0.006    0.630      0.053    0.098      0.152    0.525     0.193     homework
                 0.000    0.000    0.000      0.000    0.000      0.000    0.503     0.168     inventory
                 0.000    0.000    0.000      0.000    0.000      0.000    0.508     0.173     modeling
Weighted Avg.    0.193    0.177    0.267      0.193    0.080      0.053    0.513     0.178     

=== Confusion Matrix ===

   a   b   c   d   e   f   <-- classified as
   1 667   1   0   0   0 |   a = database
   0 746   0   0   0   0 |   b = financial
   2 663  18   6   0   0 |   c = grades
   1 591  11  34   0   0 |   d = homework
   0 687   1   7   0   0 |   e = inventory
   0 698   0   7   0   0 |   f = modeling
