% rubber: set program xelatex
%\documentclass[ucs,handout]{beamer}
\documentclass[ucs,fleqn]{beamer}
%
\usepackage{inputenc}
%\usepackage[T1]{fontenc}
\usepackage[english]{babel}
\usepackage{multicol}
\usepackage{hyperref}
\usepackage{url}
\usepackage{setspace}
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{stmaryrd}
\usepackage{graphicx}
\usepackage{tikz}
\usepackage{caption}
\usepackage{subcaption}
\usepackage{verbatim}
\usepackage{listings}
\usepackage[multiple]{footmisc}
\usepackage{xltxtra}
\usepackage{xunicode}
\defaultfontfeatures{Mapping=tex-text}
\usepackage{fontspec}
\setmainfont{Ubuntu Light}
\setsansfont{Ubuntu Light}

\definecolor{number}{rgb}{1,0,0}

%
\lstset{%
	basicstyle=\scriptsize\ttfamily,
	language={Haskell},
	%numbersep=5mm, numbers=left, numberstyle=\tiny, % number style
	breaklines=true,frame=single,framexleftmargin=0mm,xleftmargin=2mm,
	inputencoding=utf8,
	%numberstyle=\scriptsize\ttfamily\color{black!80},
	aboveskip=0.3em, belowskip=0.1em,
	framextopmargin=0.1em,
	prebreak = \raisebox{0ex}[0ex][0ex]{\ensuremath{\hookleftarrow}},
	%backgroundcolor=\color{pantoneCoolGray7!20},%frameround=fttt,escapeinside=??,
	rulecolor=\color{darkgray!50},
	morekeywords={% Give key words here
		% keywords
		},
	deletekeywords={Num},
	keywordstyle=\color[rgb]{0,0,1},             % keywords
	commentstyle=\color[rgb]{0.133,0.545,0.133}, % comments
	stringstyle=\color[rgb]{0.627,0.126,0.941},   % strings
	escapechar=@,
  %moredelim=*[s][\color{orange}]{'}{'}, %Newly added line
	%literate=
    %*{'}{{{\color{orange}'#1'}}}{1},
	literate=
      {0}{{{\color{number}{0}}}}1
	    {1}{{{\color{number}{1}}}}1
	    {2}{{{\color{number}{2}}}}1
	    {3}{{{\color{number}{3}}}}1
	    {4}{{{\color{number}{4}}}}1
	    {5}{{{\color{number}{5}}}}1
	    {6}{{{\color{number}{6}}}}1
	    {7}{{{\color{number}{7}}}}1
	    {8}{{{\color{number}{8}}}}1
	    {9}{{{\color{number}{9}}}}1
	%columns=fullflexible   
}
\usepackage{tikz}
\usetikzlibrary{arrows,calc,positioning}
%\definecolor{switch}{HTML}{006996}

\setmainfont[
  BoldFont={Ubuntu}, 
  ItalicFont={Ubuntu Light Italic},
  BoldItalicFont={Ubuntu Italic}
]{Ubuntu Light}

\usetheme{haslab}
\setbeamertemplate{navigation symbols}{}
%\setbeamersize{text margin left=10mm, text margin right=10mm} 
%\setbeamertemplate{itemize/enumerate body begin}{\setlength{\leftmargini}{2pt}}
%

%\setbeamercolor{alerted text}{fg=eeng}

\title{Towards an Automated Classification of Spreadsheets}
\institute{\vspace{1em}%
  \(^\text{1}\)HASLab / INESC TEC \& Universidade do Minho\\
  \(^\text{2}\)University of Science, Vietnam National University - Ho Chi Minh%
}
\author[Jorge Mendes]{%
  Jorge Mendes%
  \and Kha N. Do%
  \and João Saraiva%
}
\date{July 4, 2016}
%
\begin{document}

\fontspec[Ligatures=TeX,
  BoldFont={Ubuntu}, 
  ItalicFont={Ubuntu Light Italic},
  BoldItalicFont={Ubuntu Italic}
]{Ubuntu Light}

{
\author{%
  \textbf{Jorge Mendes}\(^\text{1}\)%
  \and Kha N. Do\(^\text{2}\)%
  \and João Saraiva\(^\text{1}\)%
}
\usebackgroundtemplate{}
\frame[plain]{\titlepage}
\addtocounter{framenumber}{-1}
}

\begin{frame}{Introduction}
  \begin{itemize}[<+->]
    \itemsep1em
    \item Spreadsheets are widely used in many domains.
    \item Spreadsheet research may be generic or specific to a domain.
    \item Spreadsheets can be easily found \dots\\[0.3em]
      \visible<4->{\hfill\dots{} but for a given domain might not!}
    \item<5> Copora with some categorization of their spreadsheets exist.
  \end{itemize}
\end{frame}

\begin{frame}{Motivation}
  \begin{itemize}[<+->]
    \itemsep.5em
    \item Difficult to find corpora of \emph{fresh} spreadsheets usable in
      domain-specific research.
    \item Many spreadsheets are created, but:
      \begin{itemize}
        \item without domain information;
        \item not added to existing corpora.
      \end{itemize}
    \item Categorizing (many) spreadsheets is tedious and time consuming.
    \item Usual solution: the EUSES corpus.
      \begin{itemize}
        \item Already being used in spreadsheet research.
        \item But it is static; no recent spreadsheets.
      \end{itemize}
    \item How to include new spreadsheets?
      \begin{itemize}
        \item E.g., to work with state-of-the-art features.
        \item E.g., to reproduce experiments to see if previous research results
          still hold.
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Classification}
  \begin{quote}
    Technique to assign a category to a new artefact based on the model trained
    with a given data set.
  \end{quote}
  \pause
  \begin{center}
    \includegraphics[width=0.9\textwidth]{classification}
  \end{center}
\end{frame}

\begin{frame}{Classification}{Learning}
  \begin{itemize}
    \item Generation of a model from a data set.
    \item Test the model using cross-validation.
  \end{itemize}
  \pause
  \begin{center}
    \includegraphics[scale=0.75]{training}
  \end{center}
\end{frame}

\begin{frame}{The EUSES Corpus}
  \begin{itemize}
    \itemsep1em
    \item A corpus of spreadsheets from different sources.
      \pause
    \item Mostly the result of an Internet search (Google).
      \begin{itemize}
        \itemsep0.5em
          \pause
        \item Based on keywords usually associated with spreadsheets.
        \item Spreadsheets were separated according to the keyword.
          \pause
        \item Six keywords were used:\\ \emph{database}, \emph{financial},
          \emph{grades}, \emph{homework}, \emph{inventory}, \emph{modeling}
        \item An average of 734 spreadsheets per keyword.
          \pause
        \item Only the spreadsheets gathered using this method were used.
        \item Other sources didn't have enough spreadsheets.
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Feature Extraction}
  \begin{itemize}
    \itemsep.5em
    \item Spreadsheets are not directly usable by classification algorithms.
      \begin{itemize}
        \item Features must be extracted.
      \end{itemize}
      \pause
    \item Feature used: \emph{words}
      \begin{itemize}
        \itemsep0.3em
        \item The count of each word present in the contents of the cells.
        \item Each word is an attribute of the feature.
        \item Techniques from Natural Language Processing were used:
          \begin{itemize}
            \itemsep0.2em
            \item removal of stop-words;
            \item removal of low-frequency words.
          \end{itemize}
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Algorithms}
  \begin{itemize}
    \itemsep.5em
    \item Weka provides many algorithms for data mining and machine learning
      tasks, including attribute selection and classification.
      \pause
    \item The following attribute selection algorithms were used:
      \begin{itemize}
        \itemsep.2em
        \item \emph{CfsSubsetEval} + \emph{BestFirst}
        \item \emph{CorrelationAttributeEval} + \emph{Ranker}
        \item \emph{GainRatioAttributeEval} + \emph{Ranker}
        \item \emph{InfoGainAttributeEval} + \emph{Ranker}
        \item \emph{ReliefFAttributeEval} + \emph{Ranker}
      \end{itemize}
      \pause
    \item The following Weka classifiers were used:
      \begin{itemize}
        \itemsep.2em
        \item \emph{DecisionTable} -- classifier based on decision tables.
        \item \emph{J48} -- tree-based classifier imlpementing the C4.5 algorithm.
        \item \emph{REPTree} -- another tree-based classifier.
        \item \emph{NaiveBayes} -- a Naive Bayes classifier.
        \item \emph{NaiveBayesMultinomial} -- a Multinomial Naive Bayes
          classifier.
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{The Tool -- SpreadsheetClassifier}
  \begin{itemize}
    \itemsep.5em
    \item Created to automatize classification processes.
    \item Provides preprocessing of spreadsheets.
    \item Generates data sets consumable by machine learning algorithms.
      \pause
    \item Based on:
      \begin{itemize}
          \pause
        \item \textbf{Apache POI}
          \begin{itemize}
            \item A \emph{Java} library to read and write Microsoft Office files.
            \item Used for reading spreadsheet contents.
          \end{itemize}
          \pause
        \item \textbf{Weka}
          \begin{itemize}
            \item A machine learning suite, implementing many well-known algorithms.
            \item Used as a library to generate files suitable for classification.
            \item Used as a program to perform the classification.
          \end{itemize}
      \end{itemize}
      \pause
    \item Written in \emph{Java}\hspace{1pt}:
      \begin{itemize}
        \item to easily use the \emph{Apache POI} and \emph{Weka} libraries;
        \item to be portable;
        \item to allow easy reproduction of the experiments.
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Experiments}
  \only<3>{\transdissolve}
  \begin{columns}
    \begin{column}{0.5\textwidth}
      \begin{itemize}
        \itemsep.8em
        \item A data set from each of the attribute selection algorithms.
        \item<2-> Each data set is evaluated through all the algorithms.
        \item<4-> The best combination is selected.
      \end{itemize}
    \end{column}
    \begin{column}{0.5\textwidth}
      \uncover<3->{\includegraphics[width=\columnwidth]{flow.pdf}}
    \end{column}
  \end{columns}
\end{frame}

\begin{frame}{Experiments}{Results}
  \begin{itemize}
    \itemsep1em
    \item The \textbf{\emph{CfsSubsetEval}} attribute evaluator brings many improvements:
      \hspace*{-3pt}
      \begin{itemize}
        \itemsep.2em
        \item A minimum set of attributes, decreasing processing times.
        \item Best result for every algorithm, except \emph{REPTree}.
      \end{itemize}
      \pause
    \item The best overall results are achieved with the \textbf{\emph{REPTree}}
      algorithm.
      \begin{itemize}
        \itemsep.2em
        \item The absolute best with \textbf{\emph{CorrelationAttributeEval}}.
        \item Up to 89\% accuracy.
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{The Enron Corpus}
  \begin{itemize}
    \itemsep.5em
    \item Extracted from the email data set of the \emph{Enron} company.
    \item Used in this work to obtain insight on:
      \begin{itemize}
        \item the classification process;
        \item the spreadsheets in the Enron repository.
      \end{itemize}
      \pause
    \item Classification:
      \vspace*{-1em}
      \begin{center}
        \small
        \hspace*{-4ex}
        \begin{tabular}{lr}
          Prediction   & Count \\
          \hline
          database     & 2039 \\
          financial    & 3176 \\
          grades       &  448 \\
          homework     & 8915 \\
          inventory    & 1057 \\
          modeling     &   83 \\
          \hline
          Total Result & 15718 \\
          Excluded     &   210
        \end{tabular}
      \end{center}
  \end{itemize}
\end{frame}

\begin{frame}{Discussion}
  \begin{itemize}
    \itemsep.5em
    \item This is exploratory and preliminary work.
      \pause
    \item A lightweight analysis shows:
      \begin{itemize}
        \item promising results for spreadsheet classification;
        \item the process works well, but can use some optimizations;
        \item the EUSES corpus categorization is not good for real-world
          spreadsheets in the general case.
      \end{itemize}
      \pause
    \item Apache POI:
      \begin{itemize}
        \item very limited in terms of features and spreadsheet format support;
        \item requires a lot of memory for some spreadsheets.
      \end{itemize}
      \pause
    \item Weka:
      \begin{itemize}
        \item large selection of features and classification algorithms;
        \item good usability and allows to work with large data sets.
      \end{itemize}
      \pause
    \item Enron:
      \begin{itemize}
        \item the predictions are not good to ``classify'' the spreadsheets;
        \item the results are useful to get spreadsheets to complement the
          \emph{database}, \emph{financial} and \emph{inventory} ones in the
          EUSES corpus.
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Conclusion \& Future Work}
  \begin{itemize}
    \itemsep.5em
    \item This work provides an automatic classification of spreadsheets.
      \pause
    \item Evaluated the process using cross-validation on the EUSES corpus.
      \begin{itemize}
        \item Good results: up to 89\% accuracy.
      \end{itemize}
      \pause
    \item Ran on the Enron corpus.
      \begin{itemize}
        \item The EUSES corpus does not generalize to real-world spreadsheets.
      \end{itemize}
      \pause
    \item Further work is required to create a \emph{good} spreadsheet corpus.
      \begin{itemize}
        \item Applying other data mining techniques, e.g., clustering.
        \item Create groups based on the characteristics of the spreadsheets.
      \end{itemize}
  \end{itemize}
\end{frame}

%
% The End
%
\author{%
  Jorge Mendes\(^\text{1*}\)%
  \and Kha N. Do\(^\text{2}\)%
  \and João Saraiva\(^\text{1}\)%
}
\institute{\vspace{1em}%
  \(^\text{1}\)HASLab / INESC TEC \& Universidade do Minho\\
  \(^\text{2}\)University of Science, Vietnam National University - Ho Chi Minh\\[1em]
  *
    {\tiny\url{jorgemendes@di.uminho.pt}}\\[-0.5em]
    {\tiny\url{http://www.di.uminho.pt/~jorgemendes}}
  \vspace{-3.5em}
}
{
\usebackgroundtemplate{}
\frame[plain]{\titlepage}
}
%
\addtocounter{framenumber}{-1}
%
\end{document}
