\documentclass{llncs}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{url}
\usepackage{graphicx}
\usepackage{multicol}
\usepackage{enumerate}

\newcommand{\threshold}{10}

\usepackage{xspace}
\usepackage{xcolor}
\newcommand{\TODO}[1]{{\color{red}\textbf{TODO:}\xspace#1}}

%%% TITLE %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\title{Towards an Automated Classification of Spreadsheets}


\author{Jorge Mendes\inst{1}%
\and Kha N. Do\inst{2}%
\and João Saraiva\inst{1}}

\institute{%
HASLab, INESC TEC \& Universidade do Minho, Portugal\\
\email{\{jorgemendes,jas\}@di.uminho.pt},
\and
University of Science, Vietnam National University - Ho Chi Minh\\
\email{dnkha@fit.hcmus.edu.vn}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

%%%
%%% Title
%%%
\maketitle

%%%
%%% Abstract
%%%
\begin{abstract}
  Many spreadsheets in the wild do not have documentation nor categorization associated with
  them. This makes difficult to apply spreadsheet research that targets specific
  spreadsheet domains such as financial or database.

  We introduce with this paper a methodology to automatically classify spreadsheets into
  different domains. We exploit existing data mining classification
  algorithms using spreadsheet-specific features. The algorithms were trained
  and validated with cross-validation using the EUSES corpus,
  with an up to 89\% accuracy. The best algorithm was applied to
  the larger Enron corpus in order to get some insight from it and to
  demonstrate the usefulness of this work.
  
\keywords{Spreadsheets, Data Mining, Classification}
\end{abstract}



%%%
%%% Introduction
%%%
\section{Introduction}
\label{sec:introduction}

\input{intro.tex}



%%%
%%% Classification Environment
%%%
\section{Classification Environment}
\label{sec:environment}


The classification of software artifacts
\cite{Witten:2005:DMP:1205860} is usually performed in the following
steps:
%
\begin{itemize}
  \item select data to train classification algorithms
  \item preprocess the data
  \item train the algorithms
  \item evaluate the derived models
  \item classify new artifacts 
\end{itemize}

In the classification of spreadsheets we also followed these
steps. First, we sampled the EUSES corpus to obtain a training set
(Section~\ref{sec:eusescorpus}).  Then, we preprocessed the spreadsheet
data to extract features from the training set spreadsheets
(Section~\ref{sec:featureextraction}).  Next, we considered and
applied several classification algorithms to the training set to
obtain different classification models
(Section~\ref{sec:algorithms}). After, we evaluated all models using
a five fold cross validation with this sampled dataset
(Section~\ref{sec:experiments}). Then, the best classifier will be
used to classify new spreadsheet instances, namely the Enron dataset
(see Section~\ref{sec:classification}).




%%% The EUSES Spreadsheet Corpus
\subsection{The EUSES Spreadsheet Corpus}
\label{sec:eusescorpus}

The data used to classify the algorithms is extracted from the EUSES spreadsheet
corpus~\cite{EUSESCorpus}. Most of the spreadsheets in this corpus were obtained
from the Internet through searches using the Google search engine~\cite{Google},
but some of them result from other researchers or individuals.
It has a total of 5607 spreadsheet files, organized in 11 distinct categories:
%
\begin{multicols}{3}
\begin{itemize}
  \item cs101
  \item database
  \item filby
  \item financial
  \item forms3
  \item grades
  \item homework
  \item inventory
  \item jackson
  \item modeling
  \item personal
  \item[] ~ % for three columns with four items each
\end{itemize}
\end{multicols}

Some processing was already applied to this corpus. Each of these categories has
up to three directories: \emph{bad}, \emph{duplicates}, and \emph{processed}. The
\emph{bad} directories contain files that the authors of the EUSES corpus were
unable to use for some reason\footnote{This information is not clearly
specified by the authors, but range from password protected files to spreadsheets
with disruptive macros~\cite{EUSESCorpus}.}. The \emph{duplicates} directories, as the name
suggests, contain duplicate files. The \emph{processed} directories contain the
remaining files.

From the available categories, only six were kept for classification due to the
reduced number of spreadsheets in the other categories (see
Table~\ref{tbl:eusescounts}). The six categories kept are: \emph{database},
\emph{financial}, \emph{grades}, \emph{homework}, \emph{inventory}, and
\emph{modeling}. All the spreadsheets in these categories are from the Internet
searches. Moreover, only the files in the \emph{processed} directories
were taken into account for the classification, resulting in a total of 4402
spreadsheet files, with an average of 734 files per category.

\begin{table}[ht]
  \centering
  \caption{Spreadsheet file count in the EUSES corpus.}
  \label{tbl:eusescounts}
  \setlength{\tabcolsep}{12pt}
  \begin{tabular}{lrrrr}
    ~           & Total & \emph{bad} & \emph{duplicates} & \emph{processed} \\
    \hline
    cs101       &    9 &   1 &   0 &       8 \\
\bf database    &  904 &  59 & 125 & \bf 720 \\
    filby       &   45 &   0 &   0 &      45 \\
\bf financial   &  902 &  31 &  91 & \bf 780 \\
    forms3      &   26 &   0 &   0 &      26 \\
\bf grades      &  895 &  17 & 148 & \bf 731 \\
\bf homework    &  951 &  29 & 239 & \bf 683 \\
\bf inventory   &  891 &  49 &  86 & \bf 756 \\
    jackson     &   13 &   0 &   0 &      13 \\
\bf modeling    &  966 &  51 & 183 & \bf 732 \\
    personal    &    5 &   0 &   0 &       5 \\
    \hline
    Total       & 5607 & 236 & 872 & 4499
  \end{tabular}
\end{table}


%%% Feature Extraction
\subsection{Feature Extraction}
\label{sec:featureextraction}

Sets of spreadsheet files are not directly usable to train a classifier. Thus, a
preliminary step that extracts features from the spreadsheets is required.

Spreadsheets have many attributes that can be extracted as features, hence a
selection must be made. Starting from common knowledge, having in mind the
selection of attributes that could distinguish spreadsheet categories, only the
words present in cell contents were extracted. Each word is considered an
attribute, and its value for each spreadsheet is the number of occurrences of
that word in it. This makes the \emph{words} feature.

The extraction process is as follows. If a cell contains a sentence, this sentence
is split into the several words that compose it. The resulting set of words
passes then through a cleaning process, where words that are present in all the
categories are removed from the set. Moreover, words that appear in less
than~\threshold{}\% of the spreadsheets in a given category are removed from the
set of words in that category.


%%% Algorithm Selection
\subsection{Algorithm Selection}
\label{sec:algorithms}

Several algorithms are available to classify software artifacts based
on the extracted features. In order to select the one that best suits
spreadsheet classification based on the mentioned features, several
experiments were performed with Weka. The following algorithms from
the Weka suite were used in these experiments:
%
\begin{itemize}
  \item DecisionTable -- Implementation of the IDTM algorithm~\cite{DecisionTable}
  \item J48 -- Java implementation of the C4.5 algorithm to generate decision trees~\cite{C45}
  \item REPTree -- A decision tree learner
  \item NaiveBayes -- Implementation of a Naive Bayes classifier~\cite{NaiveBayes}
  \item NaiveBayesMultinomial -- Implementation of a multinomial Naive Bayes classifier~\cite{NaiveBayesMultinomial}
\end{itemize}

\section{SSClassifier: A Java/Weka-based Spreadsheet Classifier}
\label{sec:tool}

\input{tool.tex}

%%%
%%% Experiments
%%%
\section{Experiments}
\label{sec:experiments}

Several experiments were performed to select the best set of attributes and
algorithms from the ones defined in the previous section. A common flow was
defined for the several algorithms (depicted in Fig.~\ref{fig:flow}), where we
then experimented with different inputs using five-fold cross-validation.

\begin{figure}[ht]
  \centering
  \includegraphics[width=0.8\linewidth]{flow}
  \caption{Experiment flow layout.}
  \label{fig:flow}
\end{figure}

The attributes in the \emph{words} feature consist in counts of words. The words
are the ones present in the spreadsheet contents, and some filtering is required
in order to obtain better results, much like with Natural Language
Processing~(NLP). Some filtering was already applied, as described in
Section~\ref{sec:featureextraction}, namely removing words present in all
categories and that do not provide any new information (analogously to
stop words in NLP), and discarding words that appear only in a small subset of
spreadsheets from a category.

The attributes used and their order is important in order to generate better
models. Thus, a selection of the attributes and a reordering was performed to
select the best option.
%
The different sets of data based on the set of all the word counts present in
spreadsheets used are:
%
\begin{enumerate}[A]
  \item full set of data;
  \item selection using the \emph{CfsSubsetEval} attribute evaluator and \emph{BestFirst}
    search method;
  \item selection using the \emph{CorrelationAttributeEval} attribute evaluator and
    \emph{Ranker} search method;
  \item selection using the \emph{GainRatioAttributeEval} attribute evaluator and
    \emph{Ranker} search method;
  \item selection using the \emph{InfoGainAttributeEval} attribute evaluator and \emph{Ranker}
    search method;
  \item selection using the \emph{ReliefFAttributeEval} attribute evaluator and \emph{Ranker}
    search method.
\end{enumerate}
%
%\TODO{arguments given to the attribute evaluators and search methods}\\
%\TODO{description of the attribute evaluators and search methods}

Only the data set that went through \emph{CfsSubsetEval} has less attibutes. Only the
ones relative to the words,
\textit{database},
\textit{financial},
\textit{grades},
\textit{homework},
\textit{inventory},
\textit{modeling},
\textit{size}, and
\textit{west}
are kept. The other data sets (from C to F) have only the order of the
attributes changed.

After putting each of these data sets through the experiment flow, it is
possible to see that some options provide better results than others.
The results are presented in Table~\ref{tbl:wordsresults}. The best overall
algorithm, the best overall data set, and the best overall result are displayed
in bold font face.

\begin{table}[ht]
  \centering
  \caption{Five-fold cross-validation results using the \emph{words} feature.}
  \label{tbl:wordsresults}
  \setlength{\tabcolsep}{4pt}
  \begin{tabular}{lrrrrrr}
    & \multicolumn{1}{c}{A} & \multicolumn{1}{c}{\bf B} & \multicolumn{1}{c}{C}
    & \multicolumn{1}{c}{D} & \multicolumn{1}{c}{E} & \multicolumn{1}{c}{F} \\
    \hline
NaiveBayesMultinomial
    & 57.8846 & 82.3473 & 57.8846 & 57.8846 & 57.8846 & 57.8846 \\
NaiveBayes
    & 41.0046 & 50.6158 & 41.1736 & 41.1736 & 41.1736 & 41.1253 \\
J48
    & 87.8773 & 88.0705 & 87.4909 & 87.8532 & 87.829 & 87.7324 \\
\bf REPTree
    & 88.9882 & 87.8049 & \bf 89.1331 & 89.0365 & 88.9882 & 89.0848 \\
DecisionTable
    & 85.0278 & 85.4866 & 84.9070 & 84.9070 & 84.9070 & 84.9070
  \end{tabular}
\end{table}

From the results, we can notice that the algorithm with the best overall accuracy is
\emph{REPTree}, providing the best result with the C data set. The data set B,
with only 8 word attributes, improves considerably the results for the
\emph{NaiveBayesMultinomial} algorithm, with the \emph{NaiveBayes} algorithm
also encountering some improvements. However, the other algorithms suffer in
terms of accuracy, but only slightly. Nevertheless, this data set provides a lot
of improvements in terms of time for model training. All the resulting data from
this work is provided with the source code of the tool.\footnote{\url{https://bitbucket.org/SSaaPP/spreadsheet-classification/src/2259e60/paper/data/}}



%%%
%%% Classifying the Enron Corpus
%%%
\section{Classifying the Enron Corpus}
\label{sec:classification}

The Enron corpus~\cite{EnronCorpus} is an email data set that was released to
the public. This data set was processed in order to remove private and
confidential data, but many emails and respective attachments still remain.

Hermans and Murphy-Hill~\cite{ENRON-corpus} analyzed the Enron email data set
and found spreadsheets as attachments in those emails. They extracted those
spreadsheets and provided it as its own corpus\footnote{The Enron spreadsheet
corpus is available through here: www.felienne.com/enron}.

The Enron spreadsheets have been submitted through a similar process than the
one applied to the EUSES corpus in order to classify them. First, all
spreadsheets were preprocessed in order to extract the \emph{words} feature.
Some of the spreadsheets were not analyzed due to size limits or due to not
being supported by our toolset; 210 spreadsheets were left out. Then, this data
was classified using the \emph{REPTree} algorithm that was trained with the data
from the EUSES corpus after the \emph{CorrelationAttributeEval} attribute
selection process. The results are presented in Table~\ref{tbl:enronresults}.

\begin{table}[ht]
  \centering
  \caption{Results of the prediction on the Enron corpus.}
  \label{tbl:enronresults}
  \setlength{\tabcolsep}{4pt}
  \begin{tabular}{lr}
    Prediction   & Count \\
    \hline
    database     & 2039 \\
    financial    & 3176 \\
    grades       &  448 \\
    homework     & 8915 \\
    inventory    & 1057 \\
    modeling     &   83 \\
    \hline
    Total Result & 15718
\end{tabular}
\end{table}

The results of the classification of the Enron corpus are very preliminary and
need a proper validation that due to time limitations we were unable to include
in this paper. As we can notice, most of the spreadsheets are classified as
\emph{homework}. In fact, the \emph{homework} class in the EUSES original
classification includes a large set of different domains. This results in a
large vocabulary for that category in the training of the classification
algorithms. Of course, the original data set (the EUSES corpus) and its
classification, that we use to train the algorithms, does influence the results.
A proper validation of our preliminary results is needed, indeed.



%%%
%%% Discussion
%%%
\section{Discussion}
\label{sec:discussion}

The Apache POI~\cite{ApachePOI} library was used to read the spreadsheet files
and access their contents. However, it several limitations. Its Excel file
support is considerably limited, with support only for recent file formats:
%
\begin{itemize}
  \item Excel '97(-2007)
  \item 2007 OOXML
\end{itemize}
%
From the \emph{processed} spreadsheets in the EUSES corpus, 261 spreadsheets
were discarded (around~6\%) due to the lack of support for them from Apache POI.
%
Even for the supported file formats, many features are not available or are very
limited, e.g., charts and pivot tables. Thus, we were highly constrained in the
features to extract for classification.
%
The issues of using Apache POI can be solved by switching to LibreOffice Calc or
Microsoft Excel extensions\footnote{Only tools that can be used locally were
considered to avoid issues related to transfering much data across networks.},
which have better support for these file formats.
Nevertheless, Apache POI is a relatively simple point of entry for spreadsheet
analysis, thus its use in this work.

The EUSES corpus was used as a basis for this work. This corpus already has some
kind of categorization and contains many spreadsheets. However, the categories
for the spreadsheets gathered from Internet searches (i.e., the ones that were
used in this work) are not based on spreadsheet characteristics, but on keywords
that the EUSES creators thought being commonly associated with spreadsheets.
This does not make the categories invalid, but might not reflect what people
think about the spreadsheets in those categories. Moreover, some categories may
contain some overlap. For example, one expects the \emph{homework} category to
contain spreadsheets from different domains since homework can be on diverse
subjects.

Hence, two possible issues might arise:
%
\begin{itemize}
  \item the categories do not match with what one can find from a random set of
    spreadsheets;
  \item the categorization of the spreadsheets was dependent on an Internet
    search, thus the contents/categorizations might be questionable.
\end{itemize}
%
In order to overcome these issues, a large set of spreadsheets can be gathered
and then clustering algorithms be ran on them. This would allow to organize the
spreadsheets based on their characteristics. Another option is to find
spreadsheets with a clear categorization (e.g., from the intended purpose by
their creators) and then perform a new classification based on these new
spreadsheets and categories. Both of these possible solutions can provide a
better training corpus. However, the second solution might not yield a large
enough data set to apply data mining techniques.

Nevertheless, the work herein presented allows to augment the
EUSES corpus in an automated way with spreadsheets which do not have a category
associated with them, but are close to the ones already present in the corpus.

The classification model obtained from the EUSES corpus was applied to
the Enron spreadsheet repository in order to try obtaining insight on
both the classification process and the spreadsheets in the Enron
repository. The high number of \emph{homework} spreadsheets found
suggests that the EUSES classification model is inappropriate to
classify generically any spreadsheet.

The algorithms used make only a small subset of the available algorithms for
classification. This work can be easily expanded to include other algorithms.
Moreover, more than a selection and ranking of algorithms for spreadsheet
classification, this work provides a methodology and work flow to extract
features from spreadsheets, filter them, train and then test classification
algorithms.

Another close point is the selection of attributes. Much work can still be done
in order to find the best set of attributes for a classification algorithm.
Improvements are left for future work.



%%%
%%% Conclusion
%%%
\section{Conclusion}
\label{sec:conclusion}


\input{conclusions.tex}




%%%
%%% Bibliography
%%%
%\begin{thebibliography}{}
%\end{thebibliography}
\bibliographystyle{splncs03}
\bibliography{paper}

\end{document}
