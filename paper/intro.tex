

Spreadsheets are widely used at all levels of organizations. In fact,
they are used both from professional programmers at large worldwide
organizations, to non-professional programmers in small family-run
businesses. As recent research studies \cite{Jannach2014129,6976077}
and frequent reports of horror stories\footnote{Please see the
spreadsheet horror stories available
at \url{http://www.eusprig.org/horror-stories.htm}.} show,
spreadsheets are prone to errors. Recently advanced techniques have
been proposed (some of them already incorporated in regular
programming languages), in order to improve both the efficiency and
productivity of spreadsheet users. To support such ongoing research
activity, several spreadsheet corpora have been proposed in the
literature~%
\cite{EUSESCorpus,DBLP:journals/corr/abs-1009-2785,ENRON-corpus} so
that researchers can experiment their techniques in a corpus that
represent real-world spreadsheet applications. For example, the EUSES
corpus~\cite{EUSESCorpus} divides its 5607 spreadsheets in 11 distinct categories,
including \emph{finances}, \emph{databases}, etc. As a consequence,
researchers can apply their techniques to one specific application
domain of spreadsheets.

The classification of software artifacts, in particularly source
code files, are usually performed by the administrator of a
repository~\cite{Yusof:2010:CSA}. The EUSES corpus is no exception, and its
creators gathered spreadsheets from different sources and put them together in a
single repository for easy access by researchers. When dealing with large
corpora, this process can be tedious and time consuming. Thus, it is not
surprising that the large Enron spreadsheet
repository~\cite{ENRON-corpus} is not classified yet.

This paper presents the use of automated software classification
algorithms in determining the appropriate application domain for a
particular spreadsheet. We configure well-known classification
algorithms with spreadsheet-specific properties. The EUSES corpus is
used as the basis for training and testing the classification
algorithms. In this training study we considered five different
classification algorithms, which are provided by the widely used Java-based
Weka machine learning suite~\cite{Witten:2005:DMP:1205860,WEKA}. Our first experimental results show that
the best spreadsheet classification algorithms are based on decision
trees, which correctly classifies 89\% of the spreadsheets during cross-validation.

In order to perform the feature extraction and data preprocessing, we
developed a Java-based tool to interact directly with both
spreadsheets and Weka. This helps to automate the whole process:
spreadsheets have their features automatically extracted and then
packed in a file format compatible with the Weka machine learning
suite.

Having defined the best classification algorithm for spreadsheets, we
then automatically applied the classification process to the Enron repository.
We were able to: evaluate the performance of the process, get some information
about biases from the EUSES training, get some insight on the Enron corpus from
the point of view of the EUSES corpus. These results are available to the
spreadsheet research community and show that further work on this subject is
required.


The remaining of this paper is structured as follows:
 Section~\ref{sec:environment} describes the spreadsheet
 classification process: the EUSES spreadsheet corpus, the spreadsheet
 specific features used in the classification algorithms, and the five
 used classification algorithms. Section~\ref{sec:tool} briefly describes our
 spreadsheet classification framework. Section~\ref{sec:experiments}
 contains the experiments we performed and the results obtained with
 the five classification algorithms and the spreadsheet specific
 features. Section~\ref{sec:classification} presents the results of
 classifying the Enron corpus. Section~\ref{sec:discussion} discusses
 our results and, finally, we conclude with
 Section~\ref{sec:conclusion}.
