package pt.uminho.di.ssaapp;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

public class Spreadsheet implements Comparable<Spreadsheet> {
	private String filename;
	private Workbook spreadsheet;

	/**
	 * Create a new spreadsheet representation from the given filename.
	 *
	 * @param filename The path to the spreadsheet.
	 */
	public Spreadsheet(String filename) {
		this.filename = filename;
		this.spreadsheet = null;
	}

	/**
	 * Create a new spreadsheet representation from the given filename.
	 *
	 * @param filepath The path to the spreadsheet.
	 */
	public Spreadsheet(Path filepath) {
		this.filename = filepath.toString();
		this.spreadsheet = null;
	}

	/**
	 * Get the file name of the spreadsheet.
	 * @return The spreadsheet's file name.
	 */
	public String getFileName() {
		return filename;
	}

	/**
	 * Get the Apache POI representation of the spreadsheet.
	 *
	 * @return The spreadsheet.
	 */
	public Workbook getSpreadsheet () {
		return spreadsheet;
	}

	/**
	 * Open the spreadsheet.
	 *
	 * In case the spreadsheet is already opened, close it before opening.
	 *
	 * @throws InvalidFormatException
	 * @throws EncryptedDocumentException
	 *
	 * @throws IOException
	 */
	public void open() throws EncryptedDocumentException, InvalidFormatException, IOException {
		if (spreadsheet != null)
			close();

		File input = new File(filename);
		spreadsheet = WorkbookFactory.create(input);
	}

	/**
	 * Close the spreadsheet.
	 *
	 * @throws IOException
	 */
	public void close() throws IOException {
		if (spreadsheet != null){
			spreadsheet.close();
			spreadsheet = null;
		}
	}

	/**
	 * Check if the spreadsheet is closed.
	 *
	 * @return 'True' if the spreadsheet is closed; 'False' otherwise.
	 */
	public boolean isClosed() {
		return spreadsheet == null;
	}

	/**
	 * Check if spreadsheet is supported.
	 *
	 * @throws InvalidFormatException
	 * @throws EncryptedDocumentException
	 */
	public boolean isSupported() {
		boolean result = true;
		if (isClosed()) {
			try {
				open();
				close();
			} catch (Exception e) {
				result = false;
			}
		}
		return result;
	}

	public int compareTo(Spreadsheet o) {
		return filename.compareTo(o.filename);
	}

}
