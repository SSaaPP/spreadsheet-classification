package pt.uminho.di.ssaapp.util;

import java.util.HashMap;

public class Map {
	
	/**
	 * From a 'Map' with keys and frequencies, select up to 'n' entries with
	 * higher frequencies.
	 * 
	 * @param m 'Map' with frequencies.
	 * @param n Number of entries to select.
	 * @return The original 'Map' with at most 'n' entries.
	 */
	public static <K,V> java.util.Map<K,Integer> top(java.util.Map<K,Integer> m, int n) {
		java.util.Map<K, Integer> m0 = new HashMap<K, Integer>(n);
		K min = null;
		Integer minV = 0;
		
		for (K k : m.keySet()) {
			Integer v = m.get(k);
			if (min == null) {
				min = k;
				minV = v;
				m0.put(min, minV);
			} else if (m0.size() < n) {
				if (v > minV) {
					min = k;
					minV = v;
				}
				m0.put(k, v);
			} else if (v > minV) {
				m0.remove(min);
				min = k;
				minV = v;
				m0.put(min, minV);
			}
		}

		return m0;
	}

}
