package pt.uminho.di.ssaapp.util;

import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import pt.uminho.di.ssaapp.Spreadsheet;
import pt.uminho.di.ssaapp.spreadsheet.metric.Formulas;

public class SpreadsheetFunctions {
	private HashSet<Spreadsheet> spreadsheets;

	public static void main (String[] args) {
		final String usage = "usage: java pt.uminho.di.ssaapp.utils.SpreadsheetFunctions <filelist> <output>";
		if (args.length != 2) {
			System.err.println(usage);
			System.exit(1);
		}

		Path filelist = Paths.get(args[0]);
		String filepath = args[1];

		TreeSet<String> files = null;
		try {
			 files = new TreeSet<String>(Files.readAllLines(filelist, Charset.forName("UTF-8")));
		} catch (IOException e) {
			System.err.println("Error reading file list.");
			System.exit(1);
		}

		SpreadsheetFunctions sfs = new SpreadsheetFunctions();
		for (String file : files) {
			Spreadsheet s = new Spreadsheet(file);
			sfs.addSpreasheet(s);
		}
		try {
			sfs.save(filepath);
		} catch (EncryptedDocumentException | InvalidFormatException
				| IOException e) {
			System.err.println("Error processing and saving data.");
		}
	}

	public SpreadsheetFunctions() {
		spreadsheets = new HashSet<>();
	}

	public void addSpreasheet(Spreadsheet s) {
		spreadsheets.add(s);
	}

	public void save(String filepath) throws EncryptedDocumentException, InvalidFormatException, IOException {
		TreeMap<String, TreeMap<String, Integer>> spreadsheetFunctions = new TreeMap<String, TreeMap<String,Integer>>();
		TreeSet<String> functions = new TreeSet<>();

		// get functions from spreadsheets
		for (Spreadsheet s : spreadsheets) {
			if (s.isSupported()) {
				TreeMap<String, Integer> fs = new TreeMap<>();
				Formulas f = new Formulas(s);
				for (String fname : f.getFunctions()) {
					int n = fs.getOrDefault(fname, 0);
					fs.put(fname, n + 1);
					functions.add(fname);
				}
				spreadsheetFunctions.put(s.getFileName(), fs);
			} else {
				System.err.println("Warning: file '" + s.getFileName() + "' not supported.");
			}
		}

		// create the file
		PrintWriter out = new PrintWriter(filepath,"UTF-8");
		// print header
		for (String f : functions) {
			out.print("," + f);
		}
		out.println();
		// print data
		for (String s : spreadsheetFunctions.keySet()) {
			TreeMap<String, Integer> fs = spreadsheetFunctions.get(s);
			out.print(StringEscapeUtils.escapeCsv(s));
			for(String f : functions) {
				out.print(",");
				out.print(fs.getOrDefault(f,0));
			}
			out.println();
		}
		out.close();
	}
}
