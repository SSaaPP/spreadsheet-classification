package pt.uminho.di.ssaapp.spreadsheet.metric;

import org.apache.poi.ss.usermodel.Workbook;

import pt.uminho.di.ssaapp.Spreadsheet;

public class Pictures {
	private Spreadsheet spreadsheet;

	public Pictures(Spreadsheet spreadsheet) {
		this.spreadsheet = spreadsheet;
	}

	public int getImageCount() {
		Workbook s = spreadsheet.getSpreadsheet();
		int count = s.getAllPictures().size();
		return count;
	}
}
