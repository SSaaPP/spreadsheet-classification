package pt.uminho.di.ssaapp.spreadsheet.metric;

import java.util.List;

import org.apache.poi.POIXMLDocumentPart;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFChart;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import pt.uminho.di.ssaapp.Spreadsheet;

public class Charts {
	private Spreadsheet spreadsheet;

	public Charts(Spreadsheet spreadsheet) {
		this.spreadsheet = spreadsheet;
	}

	public int getChartCount() {
		Workbook s = spreadsheet.getSpreadsheet();
		int count = 0;
		if (s instanceof XSSFWorkbook) {
			List<POIXMLDocumentPart> parts = ((XSSFWorkbook)s).getRelations();
			for (POIXMLDocumentPart part : parts) {
				if (part instanceof XSSFChart)
					count++;
			}
		} else if (s instanceof HSSFWorkbook) {
			// FIXME HSSF does not have support for charts
			count = 0;
		}
		return count;
	}
}
