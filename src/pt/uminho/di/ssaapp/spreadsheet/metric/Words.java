package pt.uminho.di.ssaapp.spreadsheet.metric;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.languagetool.rules.spelling.hunspell.Hunspell;

import pt.uminho.di.ssaapp.Spreadsheet;

public class Words {
	private Spreadsheet spreadsheet;

	public Words(Spreadsheet spreadsheet) {
		this.spreadsheet = spreadsheet;
	}

	public List<String> getWords() throws IOException, EncryptedDocumentException, InvalidFormatException {
		ArrayList<String> words = new ArrayList<String>();
		int nSheets;
		boolean closed = this.spreadsheet.isClosed();
		Workbook ss;

		if (closed)
			this.spreadsheet.open();

		Hunspell hunspell = Hunspell.getInstance();
		Hunspell.Dictionary dict = hunspell.getDictionary("data/en_US");

		ss = this.spreadsheet.getSpreadsheet();

		for (nSheets = 0; nSheets < ss.getNumberOfSheets(); nSheets++) {

			Sheet sheet = ss.getSheetAt(nSheets);
			Iterator<Row> rowIter = sheet.rowIterator();

			while (rowIter.hasNext()) {
				Row row = rowIter.next();
				Iterator<Cell> cellIter = row.cellIterator();

				while (cellIter.hasNext()) {
					Cell cell = cellIter.next();

					if (Cell.CELL_TYPE_STRING == cell.getCellType())
						words.addAll(words(cell.getStringCellValue(), dict));
				}
			}
		}

		if (closed)
			this.spreadsheet.close();
		return words;
	}

	public List<List<String>> getWordsPerSheet() throws EncryptedDocumentException, InvalidFormatException, IOException {
		ArrayList<List<String>> sheets;
		int nSheets;
		int nSheet;
		boolean closed = this.spreadsheet.isClosed();
		Workbook ss;

		if (closed)
			this.spreadsheet.open();

		Hunspell hunspell = Hunspell.getInstance();
		Hunspell.Dictionary dict = hunspell.getDictionary("data/en_US");

		ss = this.spreadsheet.getSpreadsheet();
		nSheets = ss.getNumberOfSheets();
		sheets = new ArrayList<List<String>>(nSheets);

		for (nSheet = 0; nSheet < nSheets; nSheet++) {

			Sheet sheet = ss.getSheetAt(nSheet);
			Iterator<Row> rowIter = sheet.rowIterator();
			ArrayList<String> words = new ArrayList<String>();

			while (rowIter.hasNext()) {
				Row row = rowIter.next();
				Iterator<Cell> cellIter = row.cellIterator();

				while (cellIter.hasNext()) {
					Cell cell = cellIter.next();

					if (Cell.CELL_TYPE_STRING == cell.getCellType())
						words.addAll(words(cell.getStringCellValue(), dict));
				}
			}

			sheets.set(nSheet, words);
		}

		if (closed)
			this.spreadsheet.close();
		return sheets;
	}

	private static ArrayList<String> words(String str, Hunspell.Dictionary dict) {
		ArrayList<String> result = new ArrayList<String>();

		String ws[] = str.toLowerCase().split("[\\W]");
		for (String s : ws) {
			if (s.matches("\\w+") && s.matches("\\D+") && s.length() > 3 && !dict.misspelled(s)) {
				result.add(s);
			}
		}

		return result;
	}

}
