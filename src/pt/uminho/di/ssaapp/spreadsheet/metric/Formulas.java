package pt.uminho.di.ssaapp.spreadsheet.metric;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.HSSFEvaluationWorkbook;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.formula.FormulaParser;
import org.apache.poi.ss.formula.FormulaParsingWorkbook;
import org.apache.poi.ss.formula.FormulaType;
import org.apache.poi.ss.formula.ptg.AbstractFunctionPtg;
import org.apache.poi.ss.formula.ptg.AttrPtg;
import org.apache.poi.ss.formula.ptg.Ptg;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFEvaluationWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import pt.uminho.di.ssaapp.Spreadsheet;

public class Formulas {
	private Spreadsheet spreadsheet;

	public Formulas(Spreadsheet spreadsheet) {
		this.spreadsheet = spreadsheet;
	}

	public List<String> getFunctions()
			throws IOException, EncryptedDocumentException, InvalidFormatException {
		ArrayList<String> functions = new ArrayList<String>();
		boolean closed = this.spreadsheet.isClosed();
		Workbook ss;

		if (closed)
			this.spreadsheet.open();

		ss = this.spreadsheet.getSpreadsheet();

		for (int sheetIdx = 0; sheetIdx < ss.getNumberOfSheets(); ++sheetIdx) {

			Sheet sheet = ss.getSheetAt(sheetIdx);
			Iterator<Row> rowIter = sheet.rowIterator();

			while (rowIter.hasNext()) {
				Row row = rowIter.next();
				Iterator<Cell> cellIter = row.cellIterator();

				while (cellIter.hasNext()) {
					Cell cell = cellIter.next();

					if (Cell.CELL_TYPE_FORMULA == cell.getCellType()) {
						try {
							Ptg[] formula = getFormulaTokens(cell.getCellFormula(), ss, sheetIdx);
							int n = formula.length;
							for (int i=0 ; i<n ; ++i) {
								if (formula[i] instanceof AbstractFunctionPtg) {
									String fname = ((AbstractFunctionPtg)formula[i]).getName();
									functions.add(fname);
								} else if (formula[i] instanceof AttrPtg
										&& ((AttrPtg)formula[i]).isSum()) {
									functions.add("SUM");
								}
							}
						} catch (Exception e) {
						}
					}
				}
			}
		}

		if (closed)
			this.spreadsheet.close();
		return functions;
	}

	private static Ptg[] getFormulaTokens(String formula, Workbook workbook, int sheetIdx)
			throws Exception {
		FormulaParsingWorkbook fpw = null;
		if(workbook instanceof HSSFWorkbook){
			HSSFWorkbook wb = (HSSFWorkbook) workbook;
			fpw = HSSFEvaluationWorkbook.create(wb);
		} else if(workbook instanceof XSSFWorkbook) {
			XSSFWorkbook wb = (XSSFWorkbook) workbook;
			fpw = XSSFEvaluationWorkbook.create(wb);
		} else {
			throw new Exception("unexpected workbook class");
		}
		return FormulaParser.parse(formula, fpw, FormulaType.CELL, sheetIdx);
	}

}
