package pt.uminho.di.ssaapp.spreadsheet.metric;

import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFPivotTable;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import pt.uminho.di.ssaapp.Spreadsheet;

public class PivotTables {
	private Spreadsheet spreadsheet;

	public PivotTables(Spreadsheet spreadsheet) {
		this.spreadsheet = spreadsheet;
	}

	public int getPivotTableCount() {
		Workbook s = spreadsheet.getSpreadsheet();
		int count = 0;
		if (s instanceof XSSFWorkbook) {
			List<XSSFPivotTable> pivottables = ((XSSFWorkbook)s).getPivotTables();
			count = pivottables.size();
		} else if (s instanceof HSSFWorkbook) {
			// FIXME HSSF does not have support for pivot tables
			count = 0;
		}
		return count;
	}
}
