package pt.uminho.di.ssaapp.spreadsheet;

public abstract class Metric {

	public static enum Name {
		WORDS, FORMULAS, PIVOTS, PICTURES, CHARTS;

		public static Name fromString(String v) throws Exception {
			v = v.toLowerCase();
			switch (v) {
			case "words":
				return WORDS;
			case "formulas":
				return FORMULAS;
			case "pivots":
				return PIVOTS;
			case "pictures":
				return PICTURES;
			case "charts":
				return CHARTS;
			default:
				throw new Exception("unknown metric name '" + v + "'");
			}
		}
	};

}
