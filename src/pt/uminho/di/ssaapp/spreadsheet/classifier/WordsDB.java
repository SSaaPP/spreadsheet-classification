package pt.uminho.di.ssaapp.spreadsheet.classifier;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Set;
import java.util.TreeSet;

public class WordsDB extends DB {
	private Set<String> words;

	public WordsDB(String filepath) {
		super(filepath);
		words = new TreeSet<String>();
	}

	public WordsDB(String filepath, Set<String> words) {
		super(filepath);
		this.words = words;
	}
	
	public Set<String> getWords() {
		return new TreeSet<String>(words);
	}

	/**
	 * Load the data from a data base file.
	 * 
	 * @throws IOException
	 */
	public void load() throws IOException {
		Path file = Paths.get(filepath);
		words = new TreeSet<String>(Files.readAllLines(file, Charset.forName("UTF-8")));
	}

	/**
	 * Save the data to a data base file.
	 * 
	 * @throws IOException
	 */
	public void save() throws IOException {
		Path file = Paths.get(filepath);
		Files.write(file, words, Charset.forName("UTF-8"));
	}
	
	public boolean containsWord (String word) {
		return words.contains(word);
	}

}
