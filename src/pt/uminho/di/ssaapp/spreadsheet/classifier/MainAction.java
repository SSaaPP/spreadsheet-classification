package pt.uminho.di.ssaapp.spreadsheet.classifier;

public enum MainAction {
	Filter, Preprocess, Learn, Test, Classify
}
