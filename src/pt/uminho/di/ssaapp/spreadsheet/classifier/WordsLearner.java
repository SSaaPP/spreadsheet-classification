package pt.uminho.di.ssaapp.spreadsheet.classifier;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import pt.uminho.di.ssaapp.Spreadsheet;
import pt.uminho.di.ssaapp.spreadsheet.metric.Words;

public class WordsLearner extends Learner {
	private int maxWords;
	private Set<String> stopWords;

	public final static int DefaultMaxWords = 10;

	public WordsLearner() {
		super();
		maxWords = DefaultMaxWords;
		stopWords = new HashSet<String>();
	}

	public Set<String> getStopWords() {
		return new HashSet<String>(stopWords);
	}

	public void setStopWords(Set<String> stopWords) {
		this.stopWords = stopWords;
	}

	public String getName() {
		return "words";
	}

	public Object learn() throws Exception {
		Set<String> words;
		try {
			words = getWords();
		} catch (IOException e) {
			System.err.println("Error: could not learn words.");
			throw new Exception("learning failed");
		}
		return words;
	}

	public void save(String filepath, Object data) throws Exception {
		DB db = new WordsDB(filepath, (Set<String>)data);
		try {
			db.save();
		} catch (Exception e) {
			System.err.println("Error: could not save words.");
			throw new Exception("learning failed");
		}

	}

	/**
	 * Gets the words in the spreadsheets.
	 *
	 * @return The top words in the spreadsheet set.
	 * @throws IOException
	 * @throws InvalidFormatException
	 * @throws EncryptedDocumentException
	 */
	public Set<String> getWords() throws IOException, EncryptedDocumentException, InvalidFormatException {
		Map<String, Integer> wordMap = new HashMap<String, Integer>();

		for (Spreadsheet s : spreadsheets) {
			Words sWords = new Words(s);
			List<String> allWords = sWords.getWords();

			for (String word : allWords) {
				if (!stopWords.contains(word)) {
					Integer n = wordMap.getOrDefault(word, 0);
					wordMap.put(word, n + 1);
				}
			}
		}

		return pt.uminho.di.ssaapp.util.Map.top(wordMap, maxWords).keySet();
	}

	/**
	 * Gets the words in the spreadsheets, but only counting each word once per
	 * sheet.
	 *
	 * @return The top words in the spreadsheet set.
	 * @throws IOException
	 * @throws InvalidFormatException
	 * @throws EncryptedDocumentException
	 */
	public Set<String> getUniqueWords() throws IOException, EncryptedDocumentException, InvalidFormatException {
		Map<String, Integer> wordMap = new HashMap<String, Integer>();

		for (Spreadsheet s : spreadsheets) {
			pt.uminho.di.ssaapp.spreadsheet.metric.Words sWords =
					new pt.uminho.di.ssaapp.spreadsheet.metric.Words(s);
			List<List<String>> allWords = sWords.getWordsPerSheet();

			// for each sheet, add the words to the map, but only increment each
			// word by one per sheet
			for (List<String> sheetWordList : allWords) {
				// get a set of the words (i.e., no duplicates)
				Set<String> sheetWords = new TreeSet<String>(sheetWordList);
				// add the words to the map
				for (String word : sheetWords) {
					if (!stopWords.contains(word)) {
						Integer n = wordMap.getOrDefault(word, 0);
						wordMap.put(word, n + 1);
					}
				}
			}
		}

		return pt.uminho.di.ssaapp.util.Map.top(wordMap, maxWords).keySet();
	}

	public WordsLearner clone() {
		WordsLearner obj = new WordsLearner();
		obj.maxWords = maxWords;
		obj.stopWords = stopWords;
		return obj;
	}

}
