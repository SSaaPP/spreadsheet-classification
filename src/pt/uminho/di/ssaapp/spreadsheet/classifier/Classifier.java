package pt.uminho.di.ssaapp.spreadsheet.classifier;

import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import pt.uminho.di.ssaapp.Spreadsheet;
import pt.uminho.di.ssaapp.spreadsheet.Metric;
import pt.uminho.di.ssaapp.spreadsheet.metric.Charts;
import pt.uminho.di.ssaapp.spreadsheet.metric.Formulas;
import pt.uminho.di.ssaapp.spreadsheet.metric.PivotTables;
import pt.uminho.di.ssaapp.spreadsheet.metric.Words;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;

public class Classifier {
	private List<Attribute> attributes;
	private Spreadsheet spreadsheet;

	public Classifier(List<Attribute> attributes, Spreadsheet s) {
		this.attributes = attributes;
		this.spreadsheet = s;
	}

	public Instance run () throws Exception {
		TreeMap<Feature, Attribute> attributes = new TreeMap<Feature, Attribute>();
		Feature category = null;

		// load attributes
		Set<Metric.Name> metrics = new TreeSet<Metric.Name>();
		for (Attribute attr : this.attributes) {
			Feature feature = new Feature(attr);
			attributes.put(feature, attr);
			if (feature.isCategory()) {
				category = feature;
			} else if (feature.isFunction()) {
				metrics.add(Metric.Name.FORMULAS);
			} else if (feature.isWord()) {
				metrics.add(Metric.Name.WORDS);
			} else if (feature.isPivot()) {
				metrics.add(Metric.Name.PIVOTS);
			} else if (feature.isPicture()) {
				metrics.add(Metric.Name.PICTURES);
			} else if (feature.isChart()) {
				metrics.add(Metric.Name.CHARTS);
			}
		}

		// create a data set instance
		Instance inst = new DenseInstance(attributes.size());
		inst.setMissing(attributes.get(category));
		if (metrics.contains(Metric.Name.FORMULAS))
			runFunctionCount(attributes, spreadsheet, inst);
		if (metrics.contains(Metric.Name.WORDS))
			runWordCount(attributes, spreadsheet, inst);
		if (metrics.contains(Metric.Name.PIVOTS))
			runPivotCount(attributes, spreadsheet, inst);
		if (metrics.contains(Metric.Name.PICTURES))
			runImageCount(attributes, spreadsheet, inst);
		if (metrics.contains(Metric.Name.CHARTS))
			runChartCount(attributes, spreadsheet, inst);

		return inst;
	}

	private void runFunctionCount(TreeMap<Feature, Attribute> attributes, Spreadsheet s, Instance inst) throws EncryptedDocumentException, InvalidFormatException, IOException {
		List<String> functions = new Formulas(s).getFunctions();
		for (Feature f : attributes.keySet()) {
			if (f.isFunction()) {
				String function = f.getFunction();
				int count = 0;
				for (String fun : functions)
					if (fun.equals(function))
						count++;
				inst.setValue(attributes.get(f), count);
			}
		}
	}

	private void runWordCount(TreeMap<Feature, Attribute> attributes, Spreadsheet s, Instance inst) throws EncryptedDocumentException, InvalidFormatException, IOException {
		List<String> words = new Words(s).getWords();
		for (Feature f : attributes.keySet()) {
			if (f.isWord()) {
				String word = f.getWord();
				int count = 0;
				for (String w : words)
					if (w.equals(word))
						count++;
				inst.setValue(attributes.get(f), count);
			}
		}
	}

	private void runPivotCount(TreeMap<Feature, Attribute> attributes, Spreadsheet s, Instance inst) throws EncryptedDocumentException, InvalidFormatException, IOException {
		for (Feature f : attributes.keySet()) {
			if (f.isPivot()) {
				int count = new PivotTables(s).getPivotTableCount();
				inst.setValue(attributes.get(f), count);
			}
		}
	}

	private void runImageCount(TreeMap<Feature, Attribute> attributes, Spreadsheet s, Instance inst) throws EncryptedDocumentException, InvalidFormatException, IOException {
		for (Feature f : attributes.keySet()) {
			if (f.isPicture()) {
				int count = new PivotTables(s).getPivotTableCount();
				inst.setValue(attributes.get(f), count);
			}
		}
	}

	private void runChartCount(TreeMap<Feature, Attribute> attributes, Spreadsheet s, Instance inst) throws EncryptedDocumentException, InvalidFormatException, IOException {
		for (Feature f : attributes.keySet()) {
			if (f.isChart()) {
				int count = new Charts(s).getChartCount();
				inst.setValue(attributes.get(f), count);
			}
		}
	}
}
