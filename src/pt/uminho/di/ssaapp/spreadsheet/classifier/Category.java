package pt.uminho.di.ssaapp.spreadsheet.classifier;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map.Entry;

// categories based on the EUSES Spreadsheet Corpus
public enum Category {
	CS101, DATABASE, FILBY, FINANCIAL, FORMS3, GRADES, HOMEWORK, INVENTORY, JACKSON, MODELING, PERSONAL;

	private final static EnumMap<Category, String> values = new EnumMap<Category, String>(Category.class) {
		private static final long serialVersionUID = 1L;
		{
			put(CS101, "cs101");
			put(DATABASE, "database");
			put(FILBY, "filby");
			put(FINANCIAL, "financial");
			put(FORMS3, "forms3");
			put(GRADES, "grades");
			put(HOMEWORK, "homework");
			put(INVENTORY, "inventory");
			put(JACKSON, "jackson");
			put(MODELING, "modeling");
			put(PERSONAL, "personal");
		}
	};

	public static Category fromString(String v) throws Exception {
		String val = v.toLowerCase();
		for (Entry<Category, String> e : values.entrySet()) {
			if (val.equals(e.getValue()))
				return e.getKey();
		}
		throw new Exception("unknown category name '" + v + "'");
	}

	public static List<String> getValues() {
		return new ArrayList<String>(values.values());
	}

	public String toString() {
		return values.get(this);
	}
}
