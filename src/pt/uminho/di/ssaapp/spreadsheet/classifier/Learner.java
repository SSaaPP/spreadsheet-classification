package pt.uminho.di.ssaapp.spreadsheet.classifier;

import java.util.Set;
import java.util.HashSet;

import pt.uminho.di.ssaapp.Spreadsheet;

public abstract class Learner {
	protected Set<Spreadsheet> spreadsheets;

	public Learner() {
		spreadsheets = new HashSet<>();
	}

	public void addSpreadsheet(Spreadsheet spreadsheet) {
		spreadsheets.add(spreadsheet);
	}

	public void addSpreadsheet(String spreadsheet) {
		spreadsheets.add(new Spreadsheet(spreadsheet));
	}

	public abstract String getName();

	public abstract Object learn() throws Exception;

	public abstract void save(String filepath, Object data) throws Exception;

}
