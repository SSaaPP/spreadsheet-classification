package pt.uminho.di.ssaapp.spreadsheet.classifier;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

import pt.uminho.di.ssaapp.Spreadsheet;
import pt.uminho.di.ssaapp.spreadsheet.metric.Formulas;

public class FormulaLearner extends Learner {

	public FormulaLearner() {
		super();
	}

	public String getName() {
		return "formula";
	}

	public Object learn() throws Exception {
		Set<String> formulas = new TreeSet<>();
		try {
			for (Spreadsheet s : spreadsheets) {
				Formulas f = new Formulas(s);
				formulas.addAll(f.getFunctions());
			}
		} catch (IOException e) {
			System.err.println("Error: could not learn words.");
			throw new Exception("learning failed");
		}
		return formulas;
	}

	public void save(String filepath, Object data) throws Exception {
		DB db = new WordsDB(filepath, (Set<String>)data);
		try {
			db.save();
		} catch (Exception e) {
			System.err.println("Error: could not save words.");
			throw new Exception("learning failed");
		}

	}
}
