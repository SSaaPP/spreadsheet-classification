package pt.uminho.di.ssaapp.spreadsheet.classifier;

public abstract class DB {
	protected String filepath;
	
	public DB(String filepath) {
		this.filepath = filepath;
	}
	
	/**
	 * Load the data from a data base file.
	 */
	public abstract void load() throws Exception;
	
	/**
	 * Save the data to a data base file.
	 */
	public abstract void save() throws Exception;
}
