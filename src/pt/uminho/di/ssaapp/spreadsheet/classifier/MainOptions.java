package pt.uminho.di.ssaapp.spreadsheet.classifier;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;

import pt.uminho.di.ssaapp.spreadsheet.Metric;

public class MainOptions {
	private List<String> arguments;
	private boolean help;
	private Path corpus;
	private MainAction action;
	private Path fileListPath;
	private Path dbPath;
	private Path filePath;
	private Set<Metric.Name> metrics;
	private Category tag;
	private Map<Category, Path> preprocessListPath;

	public static final String flagHelp = "--help";
	public static final String flagCorpus = "--corpus=";
	public static final String flagActionFilter = "--filter";
	public static final String flagActionPreprocess = "--preprocess";
	public static final String flagActionLearn = "--learn";
	public static final String flagActionTest = "--test";
	public static final String flagActionClassify = "--classify";
	public static final String flagFileList = "--filelist=";
	public static final String flagDb = "--db=";
	public static final String flagFile = "--file=";
	public static final String flagMetrics = "--metrics=";
	public static final String flagTag = "--tag=";

	public MainOptions(String[] args) throws MainOptionsException {
		arguments = new ArrayList<String>(Arrays.asList(args));
		help = false;
		corpus = Paths.get("");
		action = MainAction.Learn;
		fileListPath = null;
		dbPath = null;
		filePath = null;
		metrics = new TreeSet<>();
		preprocessListPath = null;

		parseHelp();

		if (!help) {
			parseGlobalOptions();
			parseActions();
			switch (action) {
			case Filter:
				fileListPath = parsePath(flagFileList);
				break;
			case Preprocess:
				preprocessListPath = parsePreprocessList(flagFileList);
				filePath = parsePath(flagFile);
				metrics = parseMetrics();
				break;
			case Learn:
				fileListPath = parsePath(flagFileList);
				metrics = parseMetrics();
				tag = parseTag();
				break;
			case Test:
				break;
			case Classify:
				dbPath = parsePath(flagDb);
				fileListPath = parsePath(flagFileList);
				filePath = parsePath(flagFile);
				break;
			}
		}
	}

	public List<String> unparsedArguments() {
		return new ArrayList<String>(arguments);
	}

	public Path getCorpusPath() {
		return corpus;
	}

	public MainAction getMainAction() {
		return action;
	}

	public Path getFileListPath() {
		return fileListPath;
	}

	public Path getDbPath() {
		return dbPath;
	}

	public Path getFilePath() {
		return filePath;
	}

	public Set<Metric.Name> getMetrics() {
		return this.metrics;
	}

	public Category getTag() {
		return this.tag;
	}

	public Map<Category, Path> getPreprocessListPath() {
		return preprocessListPath;
	}

	public boolean actionIsLearn() {
		return action == MainAction.Learn;
	}

	public boolean actionIsTest() {
		return action == MainAction.Test;
	}

	public boolean actionIsClassify() {
		return action == MainAction.Classify;
	}

	public boolean showHelp() {
		return help;
	}

	private void parseHelp() {
		if (arguments.contains(flagHelp)) {
			help = true;
			arguments.remove(flagHelp);
		}
	}

	private void parseGlobalOptions() {
		for (String arg : arguments) {
			if (arg.startsWith(flagCorpus)) {
				corpus = Paths.get(arg.substring(flagCorpus.length()));
				// TODO check if the path to the file exists
				arguments.remove(arg);
				break;
			}
		}
	}

	private void parseActions() throws NoActionException, ConflictingActionsException {
		ArrayList<MainAction> actions = new ArrayList<>();
		// find arguments which indicate the action to perform
		if (arguments.contains(flagActionFilter)) {
			actions.add(MainAction.Filter);
			arguments.remove(flagActionFilter);
		}
		if (arguments.contains(flagActionPreprocess)) {
			actions.add(MainAction.Preprocess);
			arguments.remove(flagActionPreprocess);
		}
		if (arguments.contains(flagActionLearn)) {
			actions.add(MainAction.Learn);
			arguments.remove(flagActionLearn);
		}
		if (arguments.contains(flagActionTest)) {
			actions.add(MainAction.Test);
			arguments.remove(flagActionTest);
		}
		if (arguments.contains(flagActionClassify)) {
			actions.add(MainAction.Classify);
			arguments.remove(flagActionClassify);
		}
		// check if everything is OK
		if (actions.size() == 0)
			throw new NoActionException();
		if (actions.size() > 1)
			throw new ConflictingActionsException(actions);
		// set the action
		action = actions.get(0);
	}

	private Path parsePath(String flag) throws ArgumentMissingException {
		Path path = null;
		for (String arg : arguments) {
			if (arg.startsWith(flag)) {
				path = Paths.get(arg.substring(flag.length()));
				// TODO check if the path to the file exists
				arguments.remove(arg);
				break;
			}
		}
		if (path == null)
			throw new ArgumentMissingException(flag);
		return path;
	}

	private Map<Category, Path> parsePreprocessList(String flag) throws MainOptionsException {
		TreeMap<Category, Path> list = new TreeMap<Category, Path>();
		Iterator<String> it = arguments.iterator();
		while (it.hasNext()) {
			String arg = it.next();
			if (arg.startsWith(flag)) {
				String[] catpairs = arg.substring(flag.length()).split(",");
				for (String catpair : catpairs) {
					String[] cp = catpair.split(":");
					if (cp.length != 2)
						throw new MainOptionsException("ERROR: invalid category filelist");
					Category cat;
					try {
						cat = Category.fromString(cp[0]);
					} catch (Exception e) {
						throw new InvalidTagException(cp[0]);
					}
					Path path = Paths.get(cp[1]);
					list.put(cat, path);
				}
				it.remove();
			}
		}
		return list;
	}

	private Set<Metric.Name> parseMetrics() throws UnknownMetricName {
		List<String> sMetrics = new ArrayList<>();
		for (String arg : arguments) {
			if (arg.startsWith(flagMetrics)) {
				sMetrics = Arrays.asList(arg.substring(flagMetrics.length()).split(","));
				arguments.remove(arg);
				break;
			}
		}
		TreeSet<Metric.Name> metrics = new TreeSet<>();
		for (String m : sMetrics) {
			try {
				metrics.add(Metric.Name.fromString(m));
			} catch (Exception e) {
				throw new UnknownMetricName(m);
			}
		}
		return metrics;
	}

	private Category parseTag() throws InvalidTagException, NoTagException {
		Category cat = null;
		for (String arg : arguments) {
			if (arg.startsWith(flagTag)) {
				String t;
				t = arg.substring(flagTag.length());
				try {
					cat = Category.fromString(t);
				} catch (Exception e) {
					throw new InvalidTagException(t);
				}
				arguments.remove(arg);
				return cat;
			}
		}
		throw new NoTagException();
	}

	public class MainOptionsException extends Exception {

		private static final long serialVersionUID = 1L;
		private String msg;

		public MainOptionsException() {
			msg = "MainOptionsException";
		}

		public MainOptionsException(String msg) {
			this.msg = msg;
		}

		public String getMessage() {
			return msg;
		}
	}

	public class NoActionException extends MainOptionsException {

		private static final long serialVersionUID = 1L;

		public String getMessage() {
			return "No action selected.";
		}

	}

	public class ConflictingActionsException extends MainOptionsException {

		private static final long serialVersionUID = 1L;
		List<MainAction> actions;

		public ConflictingActionsException(List<MainAction> actions) {
			this.actions = actions;
		}

		public String getMessage() {
			StringBuilder str = new StringBuilder();
			str.append("The selected actions are mutually exclusive: ");
			str.append(actions.stream().map(Object::toString)
					.collect(Collectors.joining(", ")));
			str.append(".");
			return str.toString();
		}

	}

	public class ArgumentMissingException extends MainOptionsException {

		private static final long serialVersionUID = 1L;
		String argument;

		public ArgumentMissingException(String argument) {
			this.argument = argument;
		}

		public String getMessage() {
			return "Argument '" + argument + "' not provided.";
		}

	}

	public class UnknownMetricName extends MainOptionsException {

		private static final long serialVersionUID = 1L;
		String name;

		public UnknownMetricName(String name) {
			this.name = name;
		}

		public String getMessage() {
			return "Unknown metric name '" + name + "'.";
		}

	}

	public class NoTagException extends MainOptionsException {

		private static final long serialVersionUID = 1L;

		public String getMessage() {
			return "No tag selected.";
		}

	}

	public class InvalidTagException extends MainOptionsException {

		private static final long serialVersionUID = 1L;
		String name;

		public InvalidTagException(String name) {
			this.name = name;
		}

		public String getMessage() {
			return "Invalid tag '" + name + "'";
		}

	}

}
