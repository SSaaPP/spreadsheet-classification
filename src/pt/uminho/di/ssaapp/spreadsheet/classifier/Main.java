package pt.uminho.di.ssaapp.spreadsheet.classifier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import pt.uminho.di.ssaapp.Spreadsheet;
import pt.uminho.di.ssaapp.spreadsheet.Metric;
import pt.uminho.di.ssaapp.spreadsheet.classifier.MainOptions.MainOptionsException;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.core.converters.Saver;

public class Main {

	public static void main(String[] args) {
		MainAction action = MainAction.Learn;

		// process command line arguments
		MainOptions options;
		try {
			options = new MainOptions(args);
			List<String> unparsed = options.unparsedArguments();
			if (!unparsed.isEmpty()) {
				System.err.println("Warning: Some arguments weren't taken into account:");
				System.err.println("    '" + String.join("', '", unparsed) + "'");
			}
			if (options.showHelp()) {
				showUsage(System.out);
				System.exit(0);
			}
		} catch (MainOptionsException e) {
			System.err.println("An error occurred.");
			System.err.println("Error: " + e.getMessage());
			System.err.println("");
			showUsage(System.err);
			System.exit(1);
			return;
		}

		// select which action to take
		Path corpus = options.getCorpusPath();
		action = options.getMainAction();
		switch (action) {
		case Filter:
		{
			try {
				Path file = options.getFileListPath();
				filterSupportedSpreadsheets(corpus, file);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage() + ".");
				System.exit(1);
			}
			break;
		}
		case Preprocess:
		{
			try {
				Path file = options.getFilePath();
				Set<Metric.Name> metrics = options.getMetrics();
				preprocess(corpus, file, options.getPreprocessListPath(), metrics);
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage() + ".");
				System.exit(1);
			}
			break;
		}
		case Learn:
		{
			// get the list of files to process
			Set<String> learnFiles = null;
			Path file = options.getFileListPath();
			try {
				learnFiles = new TreeSet<String>(Files.readAllLines(file, Charset.forName("UTF-8")));
			} catch (IOException e) {
				System.err.println("Error: could not read file list.");
				System.exit(1);
			}
			// do the learning
			try {
				String dbName = options.getTag().toString().toLowerCase();
				learn(corpus, dbName, learnFiles, options.getMetrics());
			} catch (Exception e) {
				System.err.println("Error: " + e.getMessage() + ".");
				System.exit(1);
			}
			break;
		}
		case Test:
		{
			test();
			break;
		}
		case Classify:
		{
			Path db = options.getDbPath();
			Path filelist = options.getFileListPath();
			Path file = options.getFilePath();
			try {
				classify(db, filelist, file);
			} catch (Exception e) {
				System.err.println("Error classifying: " + e.getMessage() + ".");
				System.exit(1);
			}
			break;
		}
		}
	}

	private static void showUsage (PrintStream out) {
		out.println("Usage: classify (--help|ACTION) [OPTIONS] [ACTION_OPTIONS]");
		out.println("");
		out.println("OPTIONS");
		out.println("  --corpus=path      - root directory of the corpus where to find the files");
		out.println("ACTION");
		out.println("  --filter");
		out.println("  --preprocess");
		out.println("  --learn");
		out.println("  --test");
		out.println("  --classify");
		out.println("");
		out.println("ACTION_OPTIONS");
		out.println("=== Filter ===");
		out.println("  Print a list with the supported spreadsheet files.");
		out.println("  Options:");
		out.println("    --filelist=path  - path to the file containing a list of all spreadsheet files");
		out.println("=== Preprocess ===");
		out.println("  --filelist=cat1:filelist1,cat2:filelist2,...");
		out.println("  --file=path      - path to the output file");
		out.println("  --metrics=m1,... - comma-separated list of metrics to use");
		out.println("=== Learn ===");
		out.println("  --filelist=path  - path is the path to a file containing a list of files to learn from");
		out.println("  --metrics=m1,... - comma-separated list of metrics to use when learning");
		out.println("  --tag=category   - category of the input files:");
		showUsagePrintCategories(out);
		out.println("=== Test ===");
		out.println("  (not yet implemented)");
		out.println("=== Classify ===");
		out.println("  --db=path        - path to the ARFF file");
		out.println("  --filelist=path  - path to a file containing a list with the spreadsheets to classify");
	}

	private static void showUsagePrintCategories(PrintStream out) {
		out.print("                       ");
		int col = 23;
		List<String> vals = Category.getValues();
		int n = vals.size();
		for (String c : vals) {
			if (col + c.length() > 78) {
				out.print("\n                       ");
				col = 23;
			}
			out.print(c);
			if (n > 1)
				out.print(", ");
			col += c.length();
			n--;
		}
		out.println();
	}

	private static void filterSupportedSpreadsheets(Path corpus, Path file) throws IOException {
		Set<String> filelist = new TreeSet<String>(Files.readAllLines(file, Charset.forName("UTF-8")));
		for (String fp : filelist) {
			Spreadsheet s = new Spreadsheet(Paths.get(corpus.toString(),fp));
			if (s.isSupported()) {
				System.out.println(fp);
			} else {
				System.err.println("Warning: file '" + s.getFileName() + "' not supported.");
			}
		}
	}

	private static void preprocess (Path corpus, Path file, Map<Category, Path> files, Set<Metric.Name> metrics) throws EncryptedDocumentException, InvalidFormatException, IOException {
		Preprocessor preprocessor = new Preprocessor(metrics);
		// add spreadsheet files
		for (Entry<Category, Path> entry : files.entrySet()) {
			Set<String> filelist = new TreeSet<String>(Files.readAllLines(entry.getValue(), Charset.forName("UTF-8")));
			for (String fp : filelist) {
				Spreadsheet s = new Spreadsheet(Paths.get(corpus.toString(),fp));
				if (s.isSupported()) {
					preprocessor.addSpreadsheet(entry.getKey(), s);
				} else {
					System.err.println("Warning: file '" + s.getFileName() + "' not supported.");
				}
			}
		}
		// run the preprocessor
		Instances data = preprocessor.run();
		ArffSaver saver = new ArffSaver();
		saver.setInstances(data);
		saver.setFile(new File(file.toString()));
		saver.writeBatch();
	}

	private static void learn (Path corpus, String dbName, Set<String> learnFiles, Set<Metric.Name> metrics) throws Exception {
		if (metrics.size() == 0)
			return;

		// create learners
		ArrayList<Learner> learners = new ArrayList<>();
		for (Metric.Name mn : metrics)
			switch(mn) {
			case WORDS:
				learners.add(new WordsLearner());
				break;
			case FORMULAS:
				learners.add(new FormulaLearner());
				break;
			default:
				System.err.println("learning from " + mn.toString() + " is not supported");
				break;
			}

		// set up learners
		for (String fp : learnFiles) {
			Spreadsheet s = new Spreadsheet(Paths.get(corpus.toString(),fp));
			if (s.isSupported()) {
				for (Learner l : learners)
					l.addSpreadsheet(s);
			} else {
				System.err.println("Warning: file '" + s.getFileName() + "' not supported.");
			}
		}

		// run learners
		for (Learner l : learners) {
			Object data = l.learn();
			l.save(dbName + "-" + l.getName() + ".db", data);
		}
	}

	private static void test () {
		// TODO code to test the learning of some data set
		System.out.println("TODO");
	}

	private static void classify (Path trainingSet, Path filePath, Path file) throws Exception {
		BufferedReader reader = new BufferedReader(new FileReader(trainingSet.toString()));
		ArffLoader.ArffReader arff = new ArffLoader.ArffReader(reader);
		Instances structure = arff.getStructure();
		int nAttributes = structure.numAttributes();
		ArrayList<Attribute> attributes = new ArrayList<Attribute>();
		for (int i=0 ; i<nAttributes ; i++) {
			attributes.add(structure.attribute(i));
		}

		Set<String> classifyFiles = new TreeSet<String>(Files.readAllLines(filePath, Charset.forName("UTF-8")));

		ArffSaver saver = new ArffSaver();
		saver.setFile(new File(file.toString()));
		saver.setRetrieval(Saver.INCREMENTAL);
		saver.setStructure(structure);
		int count = 1;
		int total = classifyFiles.size();
		PrintWriter writer = new PrintWriter(file.toString() + ".map", "UTF-8");
		int correctCount = 1;
		for (String fp : classifyFiles) {
			Spreadsheet s = new Spreadsheet(fp);
			Classifier classifier = new Classifier(attributes, s);
			try {
				System.out.println(count + " - " + total);
				count++;

				Instance inst = classifier.run();
				writer.println(correctCount + "," + fp);
				saver.writeIncremental(inst);
				correctCount++;
				System.gc();
			} catch (Exception e) {
				System.err.println("Warning: file '" + s.getFileName() + "' could not be processed.");
			}
		}
		saver.writeIncremental(null);
		writer.close();
	}


























}
