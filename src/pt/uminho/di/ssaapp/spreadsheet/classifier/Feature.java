package pt.uminho.di.ssaapp.spreadsheet.classifier;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Set;
import java.util.TreeSet;

import weka.core.Attribute;

public class Feature implements Comparable<Feature> {
	public enum Type { CATEGORY, FUNCTION, WORD, PIVOT, PICTURE, CHART };

	private static final String prefixFunction = "function_";
	private static final String prefixWord = "word_";

	private Type type;
	private String name;
	private TreeSet<String> nominalValues;

	public Feature(Type type, String name) {
		this.type = type;
		this.name = name;
		this.nominalValues = null;
	}

	public Feature(Type type, String name, Collection<? extends String> values) {
		this.type = type;
		this.name = name;
		nominalValues = new TreeSet<String>(values);
	}

	public Feature(Attribute attribute) throws Exception {
		String name = attribute.name();
		this.nominalValues = null;
		if (name.equals("category")) {
			this.type = Type.CATEGORY;
			this.name = name;
			TreeSet<String> vs = new TreeSet<String>();
			for (Enumeration<Object> values = attribute.enumerateValues(); values.hasMoreElements();)
				vs.add((String)values.nextElement());
			this.nominalValues = vs;
		} else if (name.startsWith(prefixFunction)) {
			this.type = Type.FUNCTION;
			this.name = name.substring(prefixFunction.length());
		} else if (name.startsWith(prefixWord)) {
			this.type = Type.WORD;
			this.name = name.substring(prefixWord.length());
		} else if (name.equals("pivots")) {
			this.type = Type.PIVOT;
			this.name = name;
		} else if (name.equals("pictures")) {
			this.type = Type.PICTURE;
			this.name = name;
		} else if (name.equals("charts")) {
			this.type = Type.CHART;
			this.name = name;
		} else {
			throw new Exception("cannot create feature from attribute (" + name + ")");
		}
	}

	public boolean isCategory() {
		return type == Type.CATEGORY;
	}

	public boolean isFunction() {
		return type == Type.FUNCTION;
	}

	public boolean isWord() {
		return type == Type.WORD;
	}

	public boolean isPivot() {
		return type == Type.PIVOT;
	}

	public boolean isPicture() {
		return type == Type.PICTURE;
	}

	public boolean isChart() {
		return type == Type.CHART;
	}

	public String getName() {
		if (type == Type.FUNCTION)
			return prefixFunction + name;
		if (type == Type.WORD)
			return prefixWord + name;
		return name;
	}

	public String getFunction() {
		return name;
	}

	public String getWord() {
		return name;
	}

	public Set<String> getNominalValues() {
		return new TreeSet<String>(nominalValues);
	}

	public void setNominalValues(Set<String> values) {
		nominalValues.clear();
		nominalValues.addAll(values);
	}

	public Attribute makeAttribute() {
		if (nominalValues == null)
			if (isWord())
				return new Attribute(prefixWord + name);
			else if (isFunction())
				return new Attribute(prefixFunction + name);
			else
				return new Attribute(name);
		else
			return new Attribute(name, new ArrayList<String>(nominalValues));
	}

	public boolean equals(Object o) {
		if (o instanceof Feature)
			return compareTo((Feature)o) == 0;
		return false;
	}

	public int compareTo(Feature o) {
		int ctype = type.compareTo(o.type);
		if (ctype != 0)
			return ctype;
		int cname = name.compareTo(o.name);
		if (cname != 0)
			return cname;
		return 0;
	}
}
