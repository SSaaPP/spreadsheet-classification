package pt.uminho.di.ssaapp.spreadsheet.classifier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import pt.uminho.di.ssaapp.Spreadsheet;
import pt.uminho.di.ssaapp.spreadsheet.Metric;
import pt.uminho.di.ssaapp.spreadsheet.classifier.Feature.Type;
import pt.uminho.di.ssaapp.spreadsheet.metric.Charts;
import pt.uminho.di.ssaapp.spreadsheet.metric.Formulas;
import pt.uminho.di.ssaapp.spreadsheet.metric.PivotTables;
import pt.uminho.di.ssaapp.spreadsheet.metric.Words;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

public class Preprocessor {
	private TreeMap<Category, HashSet<Spreadsheet>> spreadsheets;
	private Set<Metric.Name> metrics;

	private static final double thresholdDropWord = 0.10;

	public Preprocessor(Set<Metric.Name> metrics) {
		spreadsheets = new TreeMap<>();
		this.metrics = metrics;
	}

	public void addSpreadsheet(Category cat, Spreadsheet s) {
		HashSet<Spreadsheet> ss = spreadsheets.getOrDefault(cat, new HashSet<>());
		ss.add(s);
		spreadsheets.put(cat, ss);
	}

	public Instances run () throws EncryptedDocumentException, InvalidFormatException, IOException {
		TreeMap<Feature, Attribute> attributes = new TreeMap<Feature, Attribute>();

		// mandatory attributes
		ArrayList<String> categories = new ArrayList<String>();
		for(Category category : spreadsheets.keySet())
			categories.add(category.toString());
		Feature category = new Feature(Type.CATEGORY, "category", categories);
		attributes.put(category, category.makeAttribute());

		// optional attributes
		if (metrics.contains(Metric.Name.FORMULAS)) {
			for (String fun : selectFunctions()) {
				Feature f = new Feature(Type.FUNCTION, fun);
				attributes.put(f, f.makeAttribute());
			}
		}
		if (metrics.contains(Metric.Name.WORDS)) {
			for (String word : selectWords()) {
				Feature f = new Feature(Type.WORD, word);
				attributes.put(f, f.makeAttribute());
			}
		}
		if (metrics.contains(Metric.Name.PIVOTS)) {
			Feature f = new Feature(Type.PIVOT, "pivots");
			attributes.put(f, f.makeAttribute());
		}
		if (metrics.contains(Metric.Name.PICTURES)) {
			Feature f = new Feature(Type.PICTURE, "images");
			attributes.put(f, f.makeAttribute());
		}
		if (metrics.contains(Metric.Name.CHARTS)) {
			Feature f = new Feature(Type.CHART, "charts");
			attributes.put(f, f.makeAttribute());
		}

		// populate data set
		Instances data = new Instances("spreadsheets", new ArrayList<Attribute>(attributes.values()), spreadsheets.size());
		for (Entry<Category, HashSet<Spreadsheet>> entry : spreadsheets.entrySet()) {
			for (Spreadsheet s : entry.getValue()) {
				Instance inst = new DenseInstance(attributes.size());
				inst.setValue(attributes.get(category), entry.getKey().toString());
				if (metrics.contains(Metric.Name.FORMULAS))
					runFunctionCount(attributes, s, inst);
				if (metrics.contains(Metric.Name.WORDS))
					runWordCount(attributes, s, inst);
				if (metrics.contains(Metric.Name.PIVOTS))
					runPivotCount(attributes, s, inst);
				if (metrics.contains(Metric.Name.PICTURES))
					runImageCount(attributes, s, inst);
				if (metrics.contains(Metric.Name.CHARTS))
					runChartCount(attributes, s, inst);
				data.add(inst);
			}
		}

		return data;
	}

	private void runFunctionCount(TreeMap<Feature, Attribute> attributes, Spreadsheet s, Instance inst) throws EncryptedDocumentException, InvalidFormatException, IOException {
		List<String> functions = new Formulas(s).getFunctions();
		for (Feature f : attributes.keySet()) {
			if (f.isFunction()) {
				String function = f.getFunction();
				int count = 0;
				for (String fun : functions)
					if (fun.equals(function))
						count++;
				inst.setValue(attributes.get(f), count);
			}
		}
	}

	private void runWordCount(TreeMap<Feature, Attribute> attributes, Spreadsheet s, Instance inst) throws EncryptedDocumentException, InvalidFormatException, IOException {
		List<String> words = new Words(s).getWords();
		for (Feature f : attributes.keySet()) {
			if (f.isWord()) {
				String word = f.getWord();
				int count = 0;
				for (String w : words)
					if (w.equals(word))
						count++;
				inst.setValue(attributes.get(f), count);
			}
		}
	}

	private void runPivotCount(TreeMap<Feature, Attribute> attributes, Spreadsheet s, Instance inst) throws EncryptedDocumentException, InvalidFormatException, IOException {
		for (Feature f : attributes.keySet()) {
			if (f.isPivot()) {
				int count = new PivotTables(s).getPivotTableCount();
				inst.setValue(attributes.get(f), count);
			}
		}
	}

	private void runImageCount(TreeMap<Feature, Attribute> attributes, Spreadsheet s, Instance inst) throws EncryptedDocumentException, InvalidFormatException, IOException {
		for (Feature f : attributes.keySet()) {
			if (f.isPicture()) {
				int count = new PivotTables(s).getPivotTableCount();
				inst.setValue(attributes.get(f), count);
			}
		}
	}

	private void runChartCount(TreeMap<Feature, Attribute> attributes, Spreadsheet s, Instance inst) throws EncryptedDocumentException, InvalidFormatException, IOException {
		for (Feature f : attributes.keySet()) {
			if (f.isChart()) {
				int count = new Charts(s).getChartCount();
				inst.setValue(attributes.get(f), count);
			}
		}
	}

	private TreeSet<String> selectFunctions() throws EncryptedDocumentException, InvalidFormatException, IOException {
		TreeSet<String> allFunctions = new TreeSet<String>();
		TreeMap<Category, TreeSet<String>> functions = new TreeMap<Category, TreeSet<String>>();
		// get functions for each category
		for (Entry<Category, HashSet<Spreadsheet>> catSpreadsheets : spreadsheets.entrySet()) {
			TreeSet<String> catAllFunctions = new TreeSet<>();
			// gather functions
			for (Spreadsheet s : catSpreadsheets.getValue()) {
				TreeSet<String> sFunctions = new TreeSet<String>(new Formulas(s).getFunctions());
				catAllFunctions.addAll(sFunctions);
			}
			functions.put(catSpreadsheets.getKey(), catAllFunctions);
			allFunctions.addAll(catAllFunctions);
		}
		// remove functions present in all categories
		Iterator<String> functionIt = allFunctions.iterator();
		while (functionIt.hasNext()) {
			String function = functionIt.next();
			int count = 0;
			int size = functions.keySet().size();
			// count in how many categories the function occurs
			for (Entry<Category, TreeSet<String>> catAllFunctions : functions.entrySet()) {
				if (catAllFunctions.getValue().contains(function)) {
					count++;
				}
			}
			if (count == size)
				functionIt.remove();
		}

		return allFunctions;
	}

	private TreeSet<String> selectWords() throws EncryptedDocumentException, InvalidFormatException, IOException {
		TreeSet<String> allWords = new TreeSet<String>();
		TreeMap<Category, TreeSet<String>> words = new TreeMap<Category, TreeSet<String>>();
		// get words for each category
		for (Entry<Category, HashSet<Spreadsheet>> catSpreadsheets : spreadsheets.entrySet()) {
			TreeSet<String> catAllWords = new TreeSet<>();
			TreeMap<Spreadsheet, TreeSet<String>> catWords = new TreeMap<Spreadsheet, TreeSet<String>>();
			// gather words
			for (Spreadsheet s : catSpreadsheets.getValue()) {
				TreeSet<String> sWords = new TreeSet<String>(new Words(s).getWords());
				catWords.put(s, sWords);
				catAllWords.addAll(sWords);
			}
			// drop words not present in most of the spreadsheets
			Iterator<String> it = catAllWords.iterator();
			while (it.hasNext()) {
				String word = it.next();
				double count = 0;
				double size = catWords.size();
				// count in how many spreadsheets the word occurs
				for (Entry<Spreadsheet, TreeSet<String>> sWords : catWords.entrySet()) {
					if (sWords.getValue().contains(word)) {
						count++;
					}
				}
				// remove word if occurrence is low
				if ((count / size) < thresholdDropWord)
					it.remove();
			}
			words.put(catSpreadsheets.getKey(), catAllWords);
			allWords.addAll(catAllWords);
		}
		// remove words present in all categories
		Iterator<String> wordIt = allWords.iterator();
		while (wordIt.hasNext()) {
			String word = wordIt.next();
			int count = 0;
			int size = words.keySet().size();
			// count in how many categories the word occurs
			for (Entry<Category, TreeSet<String>> catAllWords : words.entrySet()) {
				if (catAllWords.getValue().contains(word)) {
					count++;
				}
			}
			if (count == size)
				wordIt.remove();
		}

		return allWords;
	}
}
