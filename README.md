# Spreadsheet Classification

This project intends to provide enough features in order to classify
spreadsheets into different categories, with the aim to facilitate research. In
order to achieve that, several phases were delineated:

 - preprocessing
 - training
 - testing
 - classification

## Preprocessing

Preprocessing consists in traversing the spreadsheets and extracting property
values to use in the training and classification.

In order to preprocess a set of spreadsheets, the following options are
required:

`--preprocess '--filelist=CAT_FILE_LIST' '--file=OUTPUT_ARFF'`

The `--preprocess` option indicates to run the preprocessor. The `--filelist`
option is a list of comma-separated list of category/file pairs, where the
file contains a list with paths to the spreadsheets of the defined category.
The final option, `--file`, is the file name where to store the result of the
preprocessing. Example:

`--preprocess '--filelist=cs101:cs101.list,database:database.list,financial:financial.list' '--file=spreadsheets.arff'`
